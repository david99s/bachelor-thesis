**Bachelor Thesis - Technische Universität Kaiserslautern**

Supervisor: Professor Dr. Michel 

> Impact of Schema Design on the Performance of RDBMS for Automated Tuning

How to run: 

1. go to src folder and run Advisor.java
2. Follow the guideline within the code/ comments

How to read the thesis: 

1. go to writeup folder and open main.pdf
