\chapter{Partition Advisor}\label{chap4}
This chapter contains the structure and process of the implemented advisor. The goal is that for a given workload consisting of several queries, the TPC-H database is structurally modified through horizontal and vertical partitioning in such a way that the performance time is improved compared to the answering of the queries by the original database. It is important to distinguish between the two DBMSs based on their characteristics as row or column stores. First, the basic structure of the advisor is presented. Afterwards, first the vertical and horizontal partitioning for row stores and then the horizontal partitioning for column stores are introduced. Finally, the operation of query execution is described.
\\
The structure of the following advisor can be described as follows. Basically, the advisor consists of a main class that controls the complete creation of the partitions and the execution of the requests. First, a file is read in that contains the necessary information for establishing the connection. This way, the programme knows whether it is a row store (PostgreSQL) or a column store (DuckDB). This information is crucial for deciding which type of partitions to run. After the connection has been successfully established, a file consisting of the various queries can be read in. Line by line, these queries are then first used for the respective partitions. Note that the order of execution of partitioning for row stores can get manually changed. With use of the statistic table $information\textunderscore schema$ which contains important information about the respective attributes and data type of each table, the partitions get created. The partitions are stored in different statistics tables, which will get explained in more detail later on. In addition to the new name, it is also noted here which basic table they refer to and which attributes they contain. In the second step, another file is used, which is then partially executed with the corresponding partitions on the basic of the statistics. Figure \ref*{advisor} visualizes the described structure. 

\begin{figure}
		\caption{Architecture of advisor for automated partitioning }
	\label{advisor}
	\includegraphics[width=0.9\textwidth]{advisorstructure.pdf}

\end{figure}
When partitioning, a basic distinction can be made between redundant storage of particularly frequently occurring attributes and non redundant storage of these. The redundant storage of attributes in several partitions makes a performance profit possible in comparison to the variant which does not permit redundancy, since the reading costs are very small because always suitable partitions are available.  However, redundant storage and thus the additional storage space required is a disadvantage. This results in a trade off between the reading costs for performance and the storage costs of the partitions. As there is also a positive impact on performance when creating partitions without redundancy of attributes the following solution creates vertical partitions which do not contain any redundancy of attributes, besides the primary key. However, for horizontal partitions both variants, redundancy and non redundancy of attributes, are considered since the impact on performance can be very beneficial, shown in Chapter \ref{chap3}.
\section{Solution for Vertical Partitioning}
Based on the findings from Chapter \ref{chap3} it can be seen that joining tables is very time-consuming and therefore has a negative impact on performance. Therefore, in the following, an approach is chosen that allows the original table to be retained in addition to the partitions that are created as a so-called fallback. This avoids very time-consuming joins through corresponding queries, as these can then be answered by the original table. To keep the resulting additional storage costs within limits, a storage budget, a certain percentage of the basic table, is made available to the partitions. The number of attributes is limited by the amount of storage space available. This indirectly restricts the number of partitions. 
\\
\subsection{Cost Model}
Basically, a workload $q$, consisting of requests $q_i$, serves as the basis for partitioning. In order to be able to form the optimal partitions for the restriction and the workload, costs are determined.  The cost model serves as the basis for calculating partitions per origin table. In the following some fundamental definitions are made in order to understand the cost model:
\begin{align}
	curCost(q, p) = \sum_{i}^{p}(read(q,p_i)) + readorigin(q,r)\label{eq:curcost}
\end{align}
\begin{align}
		read(q,p_i) = \sum_{j}^{q}(t \cdot a_i \cdot q_j) \label{eq:readcost}
\end{align}
\begin{align}
	readorigin(q,r) = \sum_{j}^{q}(t \cdot a_r \cdot q_j) \label{eq:readcostorigin}
\end{align}
Costs of executing a query is defined as reading costs. Those costs consists of the number of tuples $t$ and number of attributes $a_i$. Therefore the reading costs to execute a given Workload $q$ to a specific partitioning schema $p$ is given in Equation \ref{eq:curcost} and \ref{eq:readcost}. $q_j$ is determined as the number of queries which apply to the specific partition $p_i$. Equation \ref{eq:readcostorigin} calculates the reading costs for queries which have to get answered by the original table $r$, consisting of $a_r$ attributes if for some queries the partition schema is not satisfying.\\
\begin{align}
	store(p) = \sum_{i}^{p}(t \cdot a_i)\label{eq:costst}
\end{align}
Cost of storing the partition schema is defined by summing up the storing costs of each individual partition $p_i$, as can be seen in Equation \ref{eq:costst}.
\begin{align}
	store(p) \leq cap \label{eq:cap}
\end{align}
Storage budget $cap$ is a certain percentage of store costs of the origin table and the partition schema is not able to violate the storage budget in Equation \ref{eq:cap}.\\
It is important to note that in this model we assume that all attributes consume one byte of storage. As a consequence we do not consider the real storage consumption of each individual attribute regarding its data type, e.g., varchar(50) consumes 50 bytes.\\
The main goal is to get the minimum reading costs for each table in the workload in order to get a suitable partition schema.
\begin{align}
	cost_{min} = min (curCost(q,p))
\end{align}
The following example illustrates the exact procedure for vertical partitioning. Assume the workload consists of the following queries.\\

$q_1:$ {\tt Select o\textunderscore orderkey, o\textunderscore custkey from orders}\\
$q_2:$ {\tt Select o\textunderscore custkey, o\textunderscore orderstatus from orders}\\
$q_3:$ {\tt Select o\textunderscore custkey, o\textunderscore totalprice from orders}\\
$q_4:$ {\tt Select o\textunderscore totalprice, o\textunderscore orderpriority from orders}\\
$q_5:$ {\tt Select o\textunderscore totalprice, o\textunderscore clerk from orders}

Resulting in the following attributes:\\

	$a_1$: {\tt o\textunderscore orderkey}\\
	$a_2$: {\tt o\textunderscore custkey}\\
	$a_3$: {\tt o\textunderscore orderstatus}\\
	$a_4$: {\tt o\textunderscore totalprice}\\
	$a_5$: {\tt o\textunderscore orderpriority}\\
	$a_6$: {\tt o\textunderscore clerk}\\

This can get displayed in an Attribute-Occurrence-matrix, in Figure \ref{AOmatrix}, where each cell $c_{ij}$ shows the occurrence of an attribute in a query, similar to the approach of Navathe et al. \cite{navathe1984vertical}: 
\begin{flalign*}
	\hspace{12pt}
	&c_{ij}=\begin{cases}1, \, & \text{if an attribute $j$ is included in query $i$}\\  
		0, \, & \text{else}
	\end{cases}  & \hfill 				
\end{flalign*}


\begin{figure}
\centering
\caption{Attribute-Occurrence Matrix (AO)}
\label{AOmatrix}
	$\begin{matrix}\label{matrix2}
		& a_1 & a_2 & a_3 & a_4 & a_5 & a_6  & app. \\
		q_1 & 1 & 1 &0 &0 &0&0&25 \\
		q_2 & 0 & 1 & 1 & 0&0&0&15\\
		q_3 & 0 & 1 &0 &1 & 0& 0&  5 \\
		q_4 & 0 &  0&0 &1&1&0&20  \\
		q_5 & 0 &  0&0 &1&0&1&30  \\
		
	\end{matrix}$

\end{figure}
In the next step we can establish an Attribute-Affinity-Matrix, Figure \ref{AAmatrix}, which shows the affinity of each attribute to each other, meaning the times two attributes occur in the same query \cite{navathe1984vertical}. 
\begin{figure}
	\centering
	\caption{Attribute-Affinity Matrix (AA)}
	\label{AAmatrix}
	$\begin{matrix}\label{matrix2}
		&a_1 & a_2 & a_3 & a_4 & a_5 & a_6   \\
		a_1 & 25 & 25 &0 &0 &0&0\\
		a_2 & 25 & 40 & 15 & 5&0&0\\
		a_3 & 0 & 15 &15 &0 & 0& 0\\
		a_4 & 0 &  5&0 &55&20&30 \\
		a_5 & 0 &  0&0 &20&20&0 \\
		a_6 & 0 &  0&0 &30&0&30\\
	
	\end{matrix}$

\end{figure}


\subsection{Greedy Algorithm}
As already introduced, only a certain percentage of the storage costs per table is made available for further partitions. The $orders$ table used in the example has 9 attributes and has 15 million tuples. In this introductory example, however, only 1000 tuples are calculated. This results in storage costs of 9000. Furthermore, the budget capacity limit is set to 60 \%. This results in a budget $cap$ of $9000 \cdot 0.6 = 5400$. Since in vertical partitioning attributes are stored together with their complete data, only 5 attributes ( $5 \cdot 1000 \leq 5400$) can be included in partitions in order to keep within the budget limit. In the following, the exact procedure is explained step by step, before it is further illustrated with the help of the example. 


\begin{enumerate}[Step 1:]
	\item The attribute with the highest affinity value is taken from the AA matrix and put into an open partition list. The attribute also gets a pointer to signalize it as the current attribute looking at. If the currently pointed attribute is already in a partition list, then check the cost when connected to its highest affinity partner. If they are improved, include the partner. If not, pass the pointer to the next attribute. 
	\item The reading costs contain the costs for reading all partitions including the read costs which get made on the base table.
	\item The attribute with the highest affinity value for the current attribute is then selected and put into the partition list and the read costs for the current partition get calculated. If the new calculated costs exceed the old ones, the last added attribute is removed from the partition list. Repeat this for all positive affinity values of the pointer.
	\item The partition list gets closed and added to the result list, storing all partition lists. Then the pointer to the attribute with the next highest affinity value to itself is selected. 
	\item Steps 1--4 are then executed sequentially for all attributes as long as the result list does not exceed the $cap$, meaning the allowed number of attributes for partitions.  
\end{enumerate}
The explained procedure will now be illustrated using the example introduced and relevant parts of the algorithms are referenced. 
\\
At the beginning the algorithm takes a list in which the attributes are ordered descending by their AA-value, $attrOrder$. Additionally, $completeList$ contains for each attribute its affinity-partner ordered descending by its AA-value. The attribute with the highest AA-value is chosen from $attrOrder$ and gets checked whether the current partition $parts$ already contains it, as can be seen in lines \ref{maxAttr},\ref{checkNode1}, \ref{checknode2} of Algorithm \ref{alg:cap}.
The attribute with the current highest AA-Value is $a_4$ with 55. This gets added to the current partition list and its current read costs get calculated: $curCost(q,p) = 9 \cdot 1000 \cdot 95 = 855000$ as there are 95 queries in total. Select the highest AA-Partner of $a_4$ which is $a_6$ from $completeList$ and calculate current cost: $curCost(q,p) = 2 \cdot 1000 \cdot 30 + 9 \cdot 1000 \cdot 65 = 645000$.  As $curCost$ improves the reading costs one can continue with picking the next best affinity value of $a_4$. Including $a_5$ the costs get updated: $curCost(q,p) = 3 \cdot 1000 \cdot 50 + 9 \cdot 1000 \cdot 45 = 555000$\\
As the costs have improved, the last positive affinity partner  of $a_4$ is picked which is $a_2$. Updating the costs: $curCost(q,p) = 4 \cdot 1000 \cdot 55 + 9 \cdot 1000 \cdot 40 = 580000$. Now the $curCost(q,p)$ have not improved, therefore $a_2$ gets dropped from the current partition list. This list is now moved forward to the result list. \\
Continuing with Step 1, the next best attribute of the whole AA-matrix, which is $a_2$ is selected. A new partition list gets created and the costs calculated. As no query refers to only one attribute the costs have not changed creating this second partition consisting only of $a_2$. Choosing the highest affinity partner of $a_2$ which is $a_1$ and include it to its partition. Updating the costs: $curCost(q,p) = 3 \cdot 1000 \cdot 50 + 2 \cdot 1000 \cdot 25 + 9 \cdot 1000 \cdot 20 = 380000$ shows an improvement. As now the budget $cap$ of 5 attributes is exhausted, the algorithm ends, getting the final partition schema of \{$a_4 , a_5 , a_6$\} and \{$a_2 , a_1$\}. If in those the primary key of the base table is not  included yet, it will be added without considering the additional storage costs. This is crucial to obtain the uniqueness of tuples. 
\begin{algorithm}
	\caption{Algorithm for VP and HP for non redundantly stored attributes}\label{alg:cap}
	\begin{algorithmic}[1]
		\Require $attrOrder, completeList$
		\Ensure 
		\State $readcosts \gets \infty$
		\State $cap \gets$ calculated boundary of storage budget
		\State $parts \gets$ list of all partition, at the beginning empty
		\For{\texttt{$i$ $in$ $attrOrder$}} \label{maxAttr}
		\If{current $parts$ already contains $i$} \label{checkNode1}
		\State continue with algorithm \ref{addExNodes}
		\Else \label{checknode2}
		\State continue with algorithm \ref{addNodes} 
		\EndIf
		\EndFor
	\end{algorithmic}
\end{algorithm}
\begin{algorithm}
	
	\caption{Continuation of Algorithm \ref{alg:cap} for included attributes}
	\label{addExNodes}
	\begin{algorithmic}[1]
		\For{each entry $m$ in $completeList$} 
		\If{$m$ equals $i$}
		\For{each entry $i2$ in $m$} \Comment{iterates through all affinity-partner of $i$}
		\If{current partition not contain $i2$}\label{ln:4}
		\State reset parts with $i2$
		\State $curCost(q,p) \gets$ calculate cost of $parts$
		\If{$cap$ is reached}
		\State reset $parts$ without $i2$ and break loop
		\EndIf
		\If{$curCost(q,p)$ improves $readcosts$}
		\State $readcosts \gets curCost(q,p)$
		\Else
		\State reset $parts$ without $i2$ and break loop
		\EndIf
		\EndIf\label{ln:15}
		\EndFor
		\EndIf
		\EndFor
	\end{algorithmic}
\end{algorithm}
\begin{algorithm}
	
	\caption{Continuation of Algorithm \ref{alg:cap} for non included attributes}
	\label{addNodes}
	\begin{algorithmic}[1]
		\For{each entry $m$ in $completeList$}
		\If{$m$ equals $i$}
		\State reset $parts$ with $i$
		\If{$cap$ is reached}
		\State reset $parts$ without $i$ and break loop
		\EndIf
		\State $curCost(q,p) \gets$ calculate cost of $parts$
		\If{$curCost(q,p)$ improves $readcosts$}
		\State $readcosts \gets curCost(q,p)$
		\For{each entry of $i2$ in $m$}
		\State follow procedure in Alg. \ref{addExNodes} lines \ref{ln:4} - \ref{ln:15}
		\EndFor
		\Else
		\State reset $parts$ without $i$
		\EndIf
		\EndIf\EndFor
	\end{algorithmic}
\end{algorithm}
\newpage
\section{Solution for Horizontal Partitioning}

In principle, the procedure for horizontal partitioning works the same as in the algorithm presented for vertical partitioning. However, based on Chapter \ref{chap3}, differences in the performance when merging horizontal partitions could be determined. PostgreSQL's performance was worse when five partitions were merged using the SQL command UNION ALL than if the corresponding query had been executed on the original table. With DuckDB, it was found that merging partitions does not have any time disadvantages, but the performance worsens significantly under queries with several where clauses. Thus, it can be summarised that with PostgreSQL, if possible, at most four different partitions are formed in order not to slow down queries that require a union of these. With DuckDB, the number of partitions or the merging of them does not play a major role. In addition, as many queries as possible that contain WHERE clauses should be avoided by having the appropriate partitions ready. 


\subsection{HP for PostgreSQL}\label{variant}
Two different scenarios for HP can be considered. One could either opt for a horizontal partitioning that is optimal for the current workload including redundantly stored where clause attributes or a weakened variant that takes into account possible requests in future workloads. \\
In the former, one would apply the various where clause combinations applicable to the current VP, creating non-disjunctive HPs for each VP and store high frequently where clause attributes redundantly in several partition. This approach would then most likely be able to directly answer many queries from the current workload. However, for slightly modified queries, the basic table would always have to be used, as it would not be possible to unify them due to the overlapping partitions. Therefore, the second variant does not allow redundancy of where clause attributes and further introduces an upper limit for horizontal partitions in order to create counter partitions. Those can be used to for unification in case of new queries.


\begin{itemize}
	\item $t_i$ = Number of Tuple of specific HP
	\item $a_i$ = Number of attributes of current vertical partition
	\item $n$ = number of united partitions 
\end{itemize}


\begin{align}
	&curCost(q,p) = \sum_{i}^{p}\sum_{j}^{q}((t_i \cdot a_i \cdot q_j) + (t_i \cdot a_i \cdot q_j \cdot n) + (q_j \cdot a_i \cdot t))\label{hpcosts}
\end{align}
Equation \ref{hpcosts} replaces Equation \ref{eq:curcost} in order to calculate the current cost which occur for the current horizontal partition. The first part is the reading cost on a partition with $a_i$ attributes and number of tuples $t_i$ according to the current horizontal partition attribute looking at. The second part is the calculation of the reading cost for those partitions which have to get united in order to answer some queries, in case, e.g., one where clause attribute from the query differs to the ones used in the partition. The third part consists of all queries which include where clause attributes which have not get partitioned at all and have therefore get executed on the origin table. The limit $cap$ used in VP as the storage budget is replaced by $k$ as the limit for the maximum number of horizontal partitions allowed. This limit is derived from the findings in Chapter \ref{chap3}, which is intended to avoid an overhead when merging horizontal partitions. Besides this slight changes the algorithms \ref{alg:cap}, \ref{addExNodes} and \ref{addNodes} can be used equally. The initial list $attrOrder$ then includes the attributes from the where clauses for each vertical partition.
\\
For the alternative including redundancy of where clause attributes in the partition schema, there is also a storage budget that specifies how much data and tables may be created in addition to the original table. When exchanging the order of execution, starting with HP the storage costs can get determined using Equation \ref{eq:coststHP}. It consists of the tuple quantity $t_i$ for the number of tuples under partitioning $p_i$ and $a_r$ for the number of attributes of the original table: 
\begin{align}
	store(p) = \sum_{i}^{p}(t_i \cdot a_r)\label{eq:coststHP}
\end{align}
Thus, analogous to algorithm \ref{duckdbHP} having an ordered list in which all where clause combinations are stored and ordered descending by their occurrence, horizontal partitions get created as long the budget is not exhausted.

\subsection{HP for DuckDB}\label{duckdbvariant}
With DuckDB, as seen in Chapter \ref{chap3} merging individual partitions have no negative impact on performance. However, it was noted that executing queries with where clauses has significant disadvantages. Therefore, it is necessary to create as many partitions as necessary through the advisor in order to have to resort as little as possible to queries on the original table in combination with where-clauses. In addition, the question arises whether the unification of horizontal partitions is necessary at all with DuckDB. Suppose a new query contains a where clause which is not included in any partition. Therefore it requires e.g., union of four partitions and adding a the new where clause in order to be executed. The alternative would be to execute the query on the original table. With PostgreSQL, it was necessary to unify the partitions so that a query would still only need to read, e.g., two attributes from the vertical partition instead of eight attributes from the original table. This is in contrast to DuckDB, where the vectorized engine only reads the relevant attributes anyway. Thus, the merging of horizontal partitions is not necessary and there is no need to create all counter partitions for a frequently requested partition. 
\\
In case of non redundancy of where clause attributes in the partition schema the Algorithms \ref{alg:cap}, \ref{addExNodes} and \ref{addNodes} can get applied and Equation \ref{hpDBcosts} with $a_j$ as the number of select attributes in the current query $q_j$ is used as well as the storage costs of Equation \ref{eq:coststHP}. 
\begin{align}
	&curCost(q,p) = \sum_{i}^{p}\sum_{j}^{q}((t_i \cdot a_j \cdot q_j) + (q_j \cdot a_j \cdot t))\label{hpDBcosts}
\end{align}

\begin{algorithm}
	\caption{Algorithm for HP - redundantly stored attributes}
	\label{duckdbHP}
	\begin{algorithmic}[1]
		\State $cap \gets value$
		\State $parts = empty$ 
		\State $store(p) \gets 0$
		\State $list \gets Map(attributes,co-occurence)$
		\State sort $list$
		\For{each entry $l$ in  $list$}
		\State reset $parts$ with $l$
		\State calculate $store(p)$
		\If{$cap$ is reached }
		\State remove $l$ from parts
		\EndIf
		\EndFor
	\end{algorithmic}
\end{algorithm}
For the case of redundancy of where clause attributes in partitions, Equation \ref{eq:coststHP} is used in Algorithm \ref{duckdbHP} to calculate the storage cost. The most frequent where clause combination is always taken and a partition is created from it until the budget limit is reached. Queries, that are not covered by these partitions, are then executed via the original table. For queries with new where clauses, this results in high performance losses, which would also have occurred if all corresponding partitions had been combined beforehand and the new where-clause applied to the entirety afterwards.

\section{Execution of Queries}

{\tt advisorstats = (tablename, tblpkey)}\\
{\tt advisorvertpartstats = (tablename, partname, attrname)}\\
{\tt advisorhorzpartstats = (tablename, partname, attrname, wert)}	

After the appropriate partition schema has been created by the advisor, it must also be ensured that when a corresponding request arrives, it is executed by the appropriate partition. For this purpose three new tables are introduced and have to get created manually before using the advisor. The table $advisorstats$ provides only the storage of the names of the tables and their primary keys. This table is mainly used to have the correct data for unique constrains of the basic tables when creating the partitions, so that the SQL commands can be executed correctly. The $advisorvertpartstats$ table links the name of the vertical partitions and their attributes to the original table. In $advisorhorzpartstats$, the horizontal partitions are then assigned to the vertical partitions along with its attributes and value ranges. Note that all table columns has character as data type. Therefore when comparing integer values from queries with partitions, the $wert$ column has to get casted as an integer. If one changes the order of execution, then obviously the entries in $advisorhorzpartstats$ are assigned to the original table name and in $advisorvertpartstats$ the horizontal partitions to the vertical partitions. 
