\chapter{Background}\label{chap2}
This chapter sets the foundation for understanding relational database management systems (RDBMS). Starting with the introduction about the organisation of data in row  and column stores, the different concepts of schema tuning are laid down afterwards. Finally, the two different RDBMS to be considered in this thesis are presented.

\section{Foundations}

In database systems there are two main principles of how to structure data in tables. Data could be organized in tuples per row in tables, a so called row store. Even though the data is represented on a logical level in rows below each other, the actual physical storage looks different. When storing the data, the individual tuples are arranged one behind the other \cite{liu2009encyclopedia}. So that the last entry of a row is followed by the first entry of the next row. This layout is often used for online transaction processing (OLTP) queries. Such queries often affect the whole tupel in order of changes which have to get made within real-time. One example is a banking transaction, in which an immediate update of the bank account is desirable, as updates which take longer time would lead to unappropriate situations in real life. Row stores saves data of one tupel all together and in order to calculate result sets iterating through the table row per row is necessary \cite{liu2009encyclopedia}.
\\
In comparison to that, so called column stores keep all values of one attribute together so that one relation gets split into tables per column. Each attribute of a relational table is therefore also physically stored separately from each other. This column-oriented approach allows only the required attributes to be loaded for queries. Column stores are often used for online analytical processing (OLAP) queries as the requested data with those mostly affects only a few attributes, so answering these queries is faster as all values of one attribute are stored together \cite{liu2009encyclopedia}. In comparison to the row store layout where all attributes have to be considered even if one is only interested in a few. Imagine a tuple with ten columns and a query plan that only needs to operate on values from a single column. In a row-oriented database, all attributes of each row would have to get accessed and thus nine columns would fill the cache unneeded and limit the number of values that fit into the cache. Consequently, the performance of such a query would suffer.
\section{Schema Tuning}
The goal of Schema Tuning is to change the database physically by dividing an logical relation into several partitions. 
There exists multiple different strategies to create partitions. In this thesis we focus on the main approaches for relational data, Horizontal and Vertical Partitioning \cite{navathe1984vertical}.
\subsection{Horizontal Partitioning}
Data is horizontally partitioned (HP) when a table is split based on the values of one or more attributes. This results in several fragments, each containing the data of a certain range of an attribute \cite{liu2009encyclopedia}. All fragments contain the same attributes as the original relation \cite{navathe1984vertical}. 

Assuming a relation $student$ consisting of five attributes, as shown in Figure \ref{fig:horizontalbsp}, get split by ID as follows:
\begin{align*}
	student_1 = \sigma_{id<4}(student)\\
	student_2 = \sigma_{id \geq 4}(student)
\end{align*}
\begin{figure}
	\caption{Example of horizontal partitioning}
	\label{fig:horizontalbsp}
	\includegraphics[width=0.9\textwidth]{horizontal.pdf}
\end{figure}

It is important to note, that those fragments can be either disjunctive as in the example or not, if some specific tuples are assigned to both fragments. Rebuilding the relation $student$ can easily be done by reunion all fragments  \cite{liu2009encyclopedia}: 
\begin{align*}
	student = student_1 \cup student_2
\end{align*} 
Here should be mentioned that there exist an important difference between the SQL-operator UNION and UNION ALL. Applying UNION also includes checking and eliminating of duplicate tuples while UNION ALL only reunite the fragments. 

\subsection{Vertical Partitioning}\label{vp}
When a tuple consisting of multiple attributes is split so that the entire tuple is distributed across multiple linked tables, one call it vertical partitioning (VP). The individual tables then always consist of only a part of the attributes of the original table \cite{liu2009encyclopedia}. It is important to note that the primary key can still be present in all tables so that the subsets can theoretically be joined together again on request in order to be able to serve the entire tuple. The relation $student$ gets vertical partitioned like shown in Figure \ref{vertikalbsp} in two tables, formally described as follows: 
\begin{align*}
	student_1 = (ID, name, major )\\
	student_2 = (ID, address, phone number)
\end{align*}
Reconstructing the original table is done by executing a join of both tables on the redundantly stored primary key $ID$ \cite{liu2009encyclopedia}.
\begin{align*}
	student_1 s1 \bowtie student_2 \text{ $s2$ $on$ $s1.ID = s2.ID$}
\end{align*}
Obviously there can be many possibilities of how to divide a relation into several partitions. The total number of different possible partitions can be described as Bell number \cite{jindal2013comparison}. 
\begin{align}\label{eq:bell}
	B_{n+1} = \sum_{k=0}^{n} {n\choose k} B_k
\end{align}
If for example a relation has 5 attributes, there are 52 possible combination to partition the relation vertically. Furthermore, storing attributes redundantly in multiple partitions can create even more possibilities. 
\begin{figure}
	\caption{Example of vertical partitioning}
	\label{vertikalbsp}
	\includegraphics[width=0.9\textwidth]{vertical.pdf}
\end{figure} 

\section{PostgreSQL}

\subsection{Structure}
PostgreSQL is an object-relational database management system (ORDBMS) \cite{postgresdoc}. It is running on all common platforms such as Unix, Linux, Windows. The base architecture of PostgreSQL consists of two cooperating programs:

\begin{itemize}
	\item A server program called postgres controls the connection to the database from the client side and controls the data within the database. All executions specified by the client are executed by postgres on the database.
	\item The user's client application which performs database operations. There are many different client
	applications, e.g., pgAdmin (current version 4)\cite{pgadmin}, which works as a webpage on which one can create, update, maintain different databases as well as display the output for queries even in very much detailed graphics showing the logical tree used \cite{postgresdoc}.\end{itemize}
PostgreSQL stores each table as a heap file consisting of fixed size pages. A heap file is an unsorted list consisting of pages on which entries are stored. These entries are tuples in PostgreSQL. A page has the maximum size of eight KB and contains one or more tuples. However, it should be noted that tuples cannot be stored across multiple pages. Thus, pages may still have some free space. In a page there is a header at the beginning as well as identifiers that point to the actual entries within the page. In addition, there is a TOAST table for each page if required, which splits values of tuples that would not fit completely into a page due to their size and stores them in the corresponding TOAST table \cite{postgresdoc}. Due to the described storage method which PostgreSQL uses, in which tuples are stored in one piece, PostgreSQL is a row-oriented database.\\
Next to the tables of the database itself PostgreSQL contains a wide range of statistics about the used database. PostgreSQL offers an $information\textunderscore schema$ which is of particular importance for this work as it contains information about all tables and each attributes name and data type in a database. Furthermore $pg\textunderscore statistics$ contains several data about the number, frequency and distribution of each attribute \cite{postgresdoc}.

\subsection{Query Execution}
The execution of a query in PostgreSQL takes place in five steps. 
\begin{itemize}
	\item In general, a connection from the web application, for example pgAdmin 4, to the server must be opened. pgAdmin then passes the query to the server and listens for the results sent back from the server. 
	\item The parser step then checks the sent query for syntax and subsequently generates a query tree \cite{postgresdoc}.
	\item The rewrite system then takes the generated request tree and checks the system catalog for rules stored there that may need to be applied to the request tree. If this is the case, the tree is modified according to the rules.
	One of the main tasks of the rewrite system is to convert views. When a query comes to a view, it is modified by the rewrite system so that the original tables can be accessed \cite{postgresdoc}.
	\item The optimizer creates a query plan based on the tree, which is then sent to the executor. In order to be able to create the most optimal request plan, all possible paths and their costs to the result are first calculated. The most cost-effective path is then passed on to the executor \cite{postgresdoc}.  
	\item Finally, the executor recursively goes through the query plan and keeps calling the nodes of the query plan. Those nodes contain the actual task which is applied on the rows, e.g., either a sequential scan, a merge join or sorting. If one subplan is finished the node returns NULL. The tree structure of very complexe queries can consist of many levels of plan nodes and provide several different execution techniques, e.g., first Hash-join then sorting and afterwards sequential scan, which are performed by the executor. In order to be able to output result tuples simultaneously at the end the executor makes uses of the storage system while executing \cite{postgresdoc}.
\end{itemize}
As mentioned, inside the query plan nodes contain different tasks which have specific impact on how data is read. A sequential scan simply reads the heap file in order. This causes the problem when only a few rows satisfy the query the entire table must be scanned. However using indexes can speed up finding the requested data. PostgreSQL provides several index types like B-tree, Hash or Block-Range-Index (BRIN). By default PostgreSQL creates automatically an index on unique constraints, e.g., primary keys. For attributes referencing to foreign relationships, like foreign keys, are indexes not automatically created \cite{postgresdoc}. BRIN-Index works as a layer above the blocks (group of pages) retrieved from disk. The index summaries one block as a range from min to max value from data containing in the block. The used principle behind this is called min-max index which keeps track of the min and max values in consecutive groups of blocks, first initiated in 2013 by Herrera \cite{minmaxindex}.
Next to indexes the query plan also provides the selection between the different joins. PostgreSQL is using three different joins, Nested-Loop-Join, Hash-Join and the Merge-Join. One of them is chosen depending on the query \cite{postgresdoc}.


\section{DuckDB}\label{sec:duckdb}
\subsection{Structure}
Unlike PostgreSQL, DuckDB\cite{duckdbdoc} does not consist of a client/server model in which an installation of software for the server is required. With DuckDB, the database system is an importable library into a program running on the client side in a host connection, to avoid possible overheads by transferring data through a network connection. Multi-Version-Concurrency control is also provided by DuckDB, which enables multiple views on the same data set when execution of queries takes some time. DuckDB stores data on disk in a single-file storage format \cite{Raasveldt2020DataMF}. This feature is adopted from SQLite \cite{sqlite}. The storage file consists of blocks with a fixed size of 256 KB. The first block works as controller including a header to the table catalog. This table catalog provides several headers to all tables. Each table consist of a number of blocks which contain the actual data \cite{Raasveldt2020DataMF}. Blocks contain chunks of columns which are stored using light-weight compression. Inside each block all columns have a min-max index. Regarding the way DuckDB stores its data, DuckDB can be considered as a column-oriented database. Therefore, DuckDB focuses on OLAP queries without neglecting the performance of OLTP queries \cite{raasveldt2019duckdb}. Next to the min-max index, DuckDB uses an Adaptive Radix Tree (ART) index for unique constraints which get automatically created for those columns \cite{duckdbdoc}. 

\subsection{Query Execution}
The structure of DuckDB's execution engine is very close to the structure of PostgreSQL. The SQL parser is derived from the parser used by Postgres \cite{duckdbdoc}. 

\begin{itemize}
	\item The parser receives an SQL input string and converts it into a tree. The parse tree is then passed to the two-part logical planner \cite{raasveldt2019duckdb}.
	\item This consists of a so-called binder which provides the corresponding tables and views and a logical plan generator which converts the parse tree into a tree with logical operators. In addition, DuckDB keeps statistics of stored data and uses them in the optimizer \cite{raasveldt2019duckdb}. 
	\item This optimizer uses dynamic programming for calculating the best possible order of joins. It also attempts to use rewriting rules to simplify the query tree and deny sub queries, for example \cite{raasveldt2019duckdb}.
	\item With the completion of this, the optimal logical plan is available and is converted into a physical plan by the physical planner. In this process the physical planner decides between the type of scan or join \cite{raasveldt2019duckdb}.
	\item Inside the executor of DuckDB, a vectorization engine is used. This represents a middle ground between the column and row stores as described above. A vector is a fixed set of values of an attribute. A chunk consists of several vectors which form a horizontal subset of a tuple. Note that only for attributes which are relevant to the query vectors are build. The execution of the query is carried out by a so-called Volcano model. Chunks are drawn from the root node of the physical plan, which pulls recursively chunks from their child nodes till to the lowest point of the tree where a scan operator reads those chunks from the persistent table \cite{raasveldt2019duckdb}.
\end{itemize} 
A particular advantage of including the database on the client side is the efficiency of data transfer. The implemented API makes it possible that already during the query execution single chunks of the result are transferred to the client. Of course this has to be repeated until the query is finished to get all data \cite{Raasveldt2020DataMF}. 

\section{TPC-H Benchmark}
The TPC-H Benchmark \cite{tpch} is worldwide used for performance tests. In Figure \ref{fig:tpchschema} the schema is displayed including all attributes, unique constraints and relation between each tables. 
\begin{figure}
	\caption{Schema of TPC-H \cite{tpchAbb}}
	\label{fig:tpchschema}
	\includegraphics[width=1.0\textwidth]{tpch.pdf}

\end{figure}
The Scale factor (SF) is the multiplicator which can be chosen to decide how large the database can be and how many tuples get generated. By default SF=1 is chosen which generates an one GB data set. In the following for executing all queries, SF=10 will be performed to observe several phenomena which can only be manifested on a data set of proper size.


\chapter{Related Work}
Many possibilities of optimal partitioning of databases have already been presented in the literature e.g.,\cite{navathe1984vertical}, \cite{Hankins2003DataMA}, \cite{rao2002automating}, \cite{Agrawal}. Work on vertical partitioning is particularly worthy of mention. There are various approaches which are briefly mentioned and explained below. \\
Vertical partition algorithms can be divided into two main areas, top down and bottom up. In addition, the brute force can be used. This is based on the fundamental consideration of going through all the possibilities of a set of attributes and then selecting the possibility with the best possible query performance. The number of different possibilities is calculated by the Bell number introduced in Equation \ref{eq:bell}. Therefore, this approach is not really useful for relations with a high number of attributes \cite{Palatinus_2016}, \cite{Hankins2003DataMA}. \\
A well-known approach that uses the top-down approach is the work of Navathe et al., \cite{navathe1984vertical}. The basis of the optimization is the execution cost of a given workload on the partition currently under consideration. The affinity between attributes is used to construct a so-called Attribute-Affinity matrix, which can also be displayed as a graph. This matrix is then broken down into individual parts based on the execution costs until they no longer improve. The individual clusters then finally represent the vertical partitions \cite{navathe1984vertical},\cite{Palatinus_2016}.\\
The HillClimb algorithm introduced by Hankins \& Patel \cite{Hankins2003DataMA} is a popular representative of a bottom-up approach. At the beginning, it is assumed that all attributes of a relation are divided into different vertical partitions consisting only of one attribute each. Then in each iteration always two partitions are merged and afterwards the execution costs are calculated for all variants. The merged partition with the lowest cost wins and the next iteration is performed with the updated partitions. Thus, in each iteration, the number of partitions is decreased by one. This process continues as long as the cost is reduced and a better partition exists \cite{Hankins2003DataMA},\cite{Palatinus_2016}.\\
In the recent past, the Trojan algorithm, an approach based on elimination due to a boundary, has been developed. The algorithm goes through all attributes and discards those that fall below a certain threshold of interest \cite{trojan}.\\
In the context of combined vertical and horizontal partitioning as an automatic physical modification of the database, several works can be mentioned. The work of Rao et al.\cite{rao2002automating} uses the query optimizer of the respective database to find suitable partitions and to evaluate possible different combinations. Agrawal et al. \cite{Agrawal} extend this approach by taking into account the alignment of indexes to the corresponding partitions, as this makes the backup of databases easier.\\
Finally, it can be noted that except for the Trojan approach, all approaches create partitions that do not allow redundant storage of attributes in different partitions. Therefore, it is of great interest to investigate the possibility of redundant storage of attributes in comparison to disjunctive partitions.




