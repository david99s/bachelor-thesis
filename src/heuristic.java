import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class heuristic {
    public static void main(String[] args){
        /*In dieser Klasse befinden sich die die Greedy Algorithmen sowie Kostenberechnungen*/
    }
    public static ArrayList<Integer> getOrderMax(int[][] matrix){
        ArrayList<Integer> res = new ArrayList<>();
        ArrayList<Integer> cur = new ArrayList<>();
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix.length;j++){
                if(i==j){
                    cur.add(matrix[i][j]);
                }
            }
        }
        Collections.sort(cur, Collections.reverseOrder());
        return cur;
   }
    public static ArrayList<Integer> getIDMax(int[][] matrix, ArrayList<Integer> res){
        HashMap<Integer, Integer> map = new HashMap<>();
        ArrayList<Integer> id = new ArrayList<>();
        int counter=0;
        for(Integer m : res){
            for(int i=0;i<matrix.length;i++){
                for(int j=0;j<matrix.length;j++){
                    if(m.equals(matrix[i][j])&& i==j && counter<matrix.length && !id.contains(i)){
                        counter++;
                        id.add(i);
                    }
                }
            }
        }
    return id;
    }
 
    //Methode die für das aktuelle Attribut dessen beste Affnity-werte sortiert ausgibt bzw deren IDs dann
    public static HashMap<Integer, ArrayList<Integer>> innerMax(ArrayList<Integer> id, int[][] matrix){
        
        HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();
        
        for(Integer i : id){
            ArrayList<Integer> innerList = new ArrayList<>();
            ArrayList<Integer> res = new ArrayList<>();
            for(int j=0;j<matrix[i].length;j++){
                if(matrix[i][j]>0 && j != i){
                    innerList.add(matrix[i][j]);
                }
            }
            Collections.sort(innerList, Collections.reverseOrder());
            for(Integer z : innerList){
                for(int j=0;j<matrix[i].length;j++){
                    if(matrix[i][j]==z && j != i && !res.contains(j)){
                        res.add(j);
                    }
                }
            }

            map.put(i, res);
        }
        return map;
    }

    //erstellt Partitionen, die getIDMax-Liste muss durchgelaufen werden um in der richtigen Reihenfolge die entsprechenden innerlists der map durchzuführen
    public static ArrayList<ArrayList<Integer>> createPart(Connection conn, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, ArrayList<Integer> reihenfolge, HashMap<Integer, ArrayList<Integer>> completeList, String s, HashMap<Integer, String> idattr)throws SQLException{
        long curcosts=0;
        long readcosts=2100000000;
        int counter=0;
        int tupel=Tablestats.getNoTupel(conn, s);

        tupel=tupel/1000;

        int tblsize=Tablestats.getNoAttr(conn, s);
        long cap = Tablestats.storageBudget(tblsize, tupel);

        int counterAtt=0;
        ArrayList<Integer> sp = new ArrayList<>();
        ArrayList<ArrayList<Integer>> parts = new ArrayList<>();

        
            loop:
            for(Integer i : reihenfolge){
                int pos=0;
                ArrayList<Integer> sn = new ArrayList<>();
                if(checkInnerParts(parts, i)){
                    sn=correctPart(parts, i);
                    pos=getcurCounter(sn, parts);
                    outerloop:
                    for(Map.Entry<Integer, ArrayList<Integer>> m : completeList.entrySet()){
                        if(i.equals(m.getKey())){
                            for(Integer si : m.getValue()){
                                if(!sn.contains(si)&& !checkInnerParts(parts, si)){
                                    sn.add(si);
                                    parts.set(pos, sn);
                                    curcosts=calcCosts(conn, parts, store, s, idattr);
                                    counterAtt++;
                                    if(checkCap(counterAtt, cap)==false){
                                        sn.remove(si);
                                        parts.set(counter, sn);
                                        break loop;
                                    }
                                    if(readcosts>=curcosts){
                                        readcosts=curcosts;
                                        
                                    }else{
                                        sn.remove(si);
                                        parts.set(pos, sn);
                                        counterAtt--;
                                        break outerloop;
                                    } 
                                }
                            }
                        }
                    }
                } else {
                    parts.add(sn);

                    outerloop:
                    for(Map.Entry<Integer, ArrayList<Integer>> m : completeList.entrySet()){
                        if(i.equals(m.getKey())){
                            sn.add(m.getKey());
                            parts.set(counter, sn);
                            counterAtt++;
                            if(checkCap(counterAtt, cap)==false){
                                sn.remove(m.getKey());
                                parts.set(counter, sn);
                                break loop;
                            }
                            curcosts=calcCosts(conn, parts, store, s, idattr);
                            if(readcosts>=curcosts){
                                readcosts=curcosts;
                                
                                for(Integer si : m.getValue()){
                                    if(!checkInnerParts(parts, si)){
                                        sn.add(si);
                                        parts.set(counter, sn);
                                        curcosts=calcCosts(conn, parts, store, s, idattr);
                                        counterAtt++;
                                        if(checkCap(counterAtt, cap)==false){
                                            sn.remove(si);
                                            parts.set(counter, sn);
                                            break loop;
                                        }
                                        if(readcosts>=curcosts){
                                            readcosts=curcosts;
                                            
                                        }else{
                                            sn.remove(si);
                                            parts.set(counter, sn);
                                            counterAtt--;
                                            break outerloop;
                                        }
                                    }
                                }
                                
                            }else{
                                parts.remove(sn);
                                counter--;
                            }
                            
                        }
                                                
                    }
                    counter++;
                } 
                

            }

        return parts;
    }
    public static ArrayList<ArrayList<Integer>> createPartReverse(Connection conn, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, ArrayList<Integer> reihenfolge, HashMap<Integer, ArrayList<Integer>> completeList, String s, HashMap<Integer, String> idattr)throws SQLException{
        long curcosts=0;
        long readcosts=2100000000;
        int counter=0;
        int tupel=Tablestats.getNoTupel(conn, s);

        tupel=tupel/1000;

        int tblsize=Tablestats.getNoAttr(conn, s);
        long cap = tblsize;

        int counterAtt=0;
        ArrayList<Integer> sp = new ArrayList<>();
        ArrayList<ArrayList<Integer>> parts = new ArrayList<>();

        
            loop:
            for(Integer i : reihenfolge){
                int pos=0;
                ArrayList<Integer> sn = new ArrayList<>();
                if(checkInnerParts(parts, i)){
                    sn=correctPart(parts, i);
                    pos=getcurCounter(sn, parts);
                    outerloop:
                    for(Map.Entry<Integer, ArrayList<Integer>> m : completeList.entrySet()){
                        if(i.equals(m.getKey())){
                            for(Integer si : m.getValue()){
                                if(!sn.contains(si)&& !checkInnerParts(parts, si)){
                                    sn.add(si);
                                    parts.set(pos, sn);
                                    //System.out.println(parts);
                                    curcosts=calcCosts(conn, parts, store, s, idattr);
                                    counterAtt++;
                                    if(checkCap(counterAtt, cap)==false){
                                        sn.remove(si);
                                        parts.set(counter, sn);
                                        break loop;
                                    }
                                    //System.out.println(curcosts);
                                    if(readcosts>=curcosts){
                                        readcosts=curcosts;
                                        
                                    }else{
                                        sn.remove(si);
                                        parts.set(pos, sn);
                                        counterAtt--;
                                        break outerloop;
                                    } 
                                }
                            }
                        }
                    }
                } else {
                    parts.add(sn);
                    //System.out.println(parts);
                    outerloop:
                    for(Map.Entry<Integer, ArrayList<Integer>> m : completeList.entrySet()){
                        if(i.equals(m.getKey())){
                            sn.add(m.getKey());
                            parts.set(counter, sn);
                            counterAtt++;
                            if(checkCap(counterAtt, cap)==false){
                                sn.remove(m.getKey());
                                parts.set(counter, sn);
                                break loop;
                            }
                            //System.out.println(parts);
                            curcosts=calcCosts(conn, parts, store, s, idattr);

                            if(readcosts>=curcosts){
                                readcosts=curcosts;
                                
                                for(Integer si : m.getValue()){
                                    if(!checkInnerParts(parts, si)){
                                        sn.add(si);
                                        parts.set(counter, sn);
                                        curcosts=calcCosts(conn, parts, store, s, idattr);
                                        counterAtt++;
                                        if(checkCap(counterAtt, cap)==false){
                                            sn.remove(si);
                                            parts.set(counter, sn);
                                            break loop;
                                        }
                                        if(readcosts>=curcosts){
                                            readcosts=curcosts;
                                            
                                        }else{
                                            sn.remove(si);
                                            parts.set(counter, sn);
                                            counterAtt--;
                                            break outerloop;
                                        }
                                    }
                                }
                                
                            }else{
                                parts.remove(sn);
                                counter--;
                            }
                            
                        }
                                                
                    }
                    counter++;
                } 
                

            }

        return parts;
    }
    public static boolean checkCap(int counterAttr, long cap){
        boolean pointer=true;
        if(counterAttr>cap){
            return pointer=false;
        } 
        return pointer;
    }
    public static boolean checkInnerParts(ArrayList<ArrayList<Integer>> parts, Integer si){
        boolean pointer = false;
        for(ArrayList<Integer> ai : parts){
            if(ai.contains(si)){
                pointer=true;
            }
        }
        return pointer;
    }
    public static ArrayList<Integer> correctPart(ArrayList<ArrayList<Integer>> parts, Integer si){
        ArrayList<Integer> rescur= new ArrayList<>();
        for(ArrayList<Integer> al : parts ){
            if(al.contains(si)){
                rescur = al;
            }
        }
        return rescur;
    }
    public static int getcurCounter(ArrayList<Integer> cur, ArrayList<ArrayList<Integer>> parts){
        int counter=0;
        for(ArrayList<Integer> al : parts){
            if(al.equals(cur)){
                counter=parts.indexOf(al);
            }
        }
        return counter;
    }
    //berechnet Kosten für aktuelle Partitionenliste
    public static long calcCosts( Connection conn, ArrayList<ArrayList<Integer>> parts, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String s, HashMap<Integer, String> idattr)throws SQLException{
        int curcosts=0;
        int numbq=0;
        long totalc=0;
        int tupel=Tablestats.getNoTupel(conn, s)/1000;
        System.out.println(tupel);
        int tblsize=Tablestats.getNoAttr(conn, s);
        System.out.println(tblsize);
        for(Map.Entry<ArrayList<String>, Integer> m : store.get(s).entrySet()){
            numbq+=m.getValue();
        }
        System.out.println(numbq);
        for(ArrayList<Integer> al : parts){
            System.out.println(al);
            ArrayList<String> cur = new ArrayList<>();
            int app=0;
            for(Integer i : al){
                for(Integer i2 : idattr.keySet()){
                    if(Integer.valueOf(i+1).equals(i2)){
                        cur.add(idattr.get(i2));
                        //System.out.println(cur);
                    }
                }
            }
            int counterk=0;
            for(Map.Entry<ArrayList<String>, Integer> m : store.get(s).entrySet()){
                if(cur.containsAll(m.getKey())){
                    app+=m.getValue();
                }
            }
            System.out.println(app);
            curcosts+=app * al.size() * tupel;
            numbq=numbq-app;
            app=0;
            System.out.println(curcosts);
            System.out.println(numbq);
        }
        
        System.out.println(tupel);
        System.out.println(numbq);
        System.out.println(tblsize);
        int rest = numbq * tblsize * tupel;
        System.out.println(rest);
        totalc = curcosts + numbq * tblsize * tupel;
        System.out.println(totalc);
        return totalc;
    }

    public static ArrayList<ArrayList<Integer>> createHortPart(Connection conn, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, ArrayList<Integer> reihenfolge, HashMap<Integer, ArrayList<Integer>> completeList, String s, HashMap<Integer, String> idattr)throws SQLException{
        long curcosts=0;
        long readcosts=1000000000;
        int counter=0;
        //int tupel=Tablestats.getNoTupel(conn, s);//muss später wo anders mit einzelnen Attributen aufgerufen werden 
        int tblsize=Tablestats.getNoAttr(conn, s);//funktioniert auch mit den aus VP erstellten Partitionen
        long cap = 2; //erklärung siehe kapitel 3, weil daraus 4 HP resultieren, grade so max für union all
        
        int counterAtt=0;
      
        ArrayList<ArrayList<Integer>> parts = new ArrayList<>();
       
        
        loop:
        for(Integer i : reihenfolge){
            int pos=0;
            ArrayList<Integer> sn = new ArrayList<>();
            if(checkInnerParts(parts, i)){
                sn=correctPart(parts, i);
                pos=getcurCounter(sn, parts);
                outerloop:
                for(Map.Entry<Integer, ArrayList<Integer>> m : completeList.entrySet()){
                    if(i.equals(m.getKey())){
                        for(Integer si : m.getValue()){
                            if(!sn.contains(si)&& !checkInnerParts(parts, si)){
                                sn.add(si);
                                parts.set(pos, sn);

                                curcosts=calcHPCosts(conn, parts, store, s, idattr);
                                counterAtt++;
                                if(checkCap(counterAtt, cap)==false){
                                    sn.remove(si);
                                    parts.set(counter, sn);
                                    break loop;
                                }

                                if(readcosts>=curcosts){
                                    readcosts=curcosts;
                                    
                                }else{
                                    sn.remove(si);
                                    parts.set(pos, sn);
                                    counterAtt--;
                                    break outerloop;
                                } 
                            }
                        }
                    }
                }
            } else {
                parts.add(sn);

                outerloop:
                for(Map.Entry<Integer, ArrayList<Integer>> m : completeList.entrySet()){
                    if(i.equals(m.getKey())){
                        sn.add(m.getKey());
                        parts.set(counter, sn);
                        counterAtt++;
                        if(checkCap(counterAtt, cap)==false){
                            sn.remove(m.getKey());
                            parts.set(counter, sn);
                            break loop;
                        }

                        curcosts=calcHPCosts(conn, parts, store, s, idattr);
                        
                        if(readcosts>=curcosts){
                            System.out.println("hi");
                            readcosts=curcosts;
                            
                            for(Integer si : m.getValue()){
                                if(!checkInnerParts(parts, si)){
                                    sn.add(si);
                                    parts.set(counter, sn);
                                    curcosts=calcHPCosts(conn, parts, store, s, idattr);
                                    System.out.println(curcosts);
                                    counterAtt++;
                                    if(checkCap(counterAtt, cap)==false){
                                        sn.remove(si);
                                        parts.set(counter, sn);
                                        break loop;
                                    }
                                    if(readcosts>=curcosts){
                                        readcosts=curcosts;
                                        
                                    }else{
                                        sn.remove(si);
                                        parts.set(counter, sn);
                                        counterAtt--;
                                        break outerloop;
                                    }
                                }
                            }
                            
                        }else{
                            parts.remove(sn);
                            counter--;
                        }
                        
                    }
                                            
                }
                counter++;
            } 

            }

 
        return parts;
    }

    public static ArrayList<ArrayList<Integer>> createHortPartReverse(Connection conn, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, ArrayList<Integer> reihenfolge, HashMap<Integer, ArrayList<Integer>> completeList, String s, HashMap<Integer, String> idattr)throws SQLException{
        long curcosts=0;
        long readcosts=1000000000;
        int counter=0;
        int tupel=Tablestats.getNoTupel(conn, s);
        int tblsize=Tablestats.getNoAttr(conn, s);
        long cap = Tablestats.storageBudgetDuck(tblsize, tupel); 
        
        int counterAtt=0;
      
        ArrayList<ArrayList<Integer>> parts = new ArrayList<>();
       
        
        loop:
        for(Integer i : reihenfolge){
            int pos=0;
            ArrayList<Integer> sn = new ArrayList<>();
            if(checkInnerParts(parts, i)){
                sn=correctPart(parts, i);
                pos=getcurCounter(sn, parts);
                outerloop:
                for(Map.Entry<Integer, ArrayList<Integer>> m : completeList.entrySet()){
                    if(i.equals(m.getKey())){
                        for(Integer si : m.getValue()){
                            if(!sn.contains(si)&& !checkInnerParts(parts, si)){
                                sn.add(si);
                                parts.set(pos, sn);
                                //System.out.println(parts);
                                curcosts=calcHPCostsReverse(conn, parts, store, s, idattr);
                                //System.out.println(curcosts);
                                if(cap>=curcosts){
                                    continue;
                                    
                                }else{
                                    sn.remove(si);
                                    parts.set(pos, sn);
                                    break outerloop;
                                } 
                            }
                        }
                    }
                }
            } else {
                parts.add(sn);
                //System.out.println(parts);
                outerloop:
                for(Map.Entry<Integer, ArrayList<Integer>> m : completeList.entrySet()){
                    if(i.equals(m.getKey())){
                        sn.add(m.getKey());
                        parts.set(counter, sn);
                        counterAtt++;
                        curcosts=calcHPCostsReverse(conn, parts, store, s, idattr);
                        if(cap<curcosts){
                            sn.remove(m.getKey());
                            parts.set(counter, sn);
                            break loop;
                        }

                        
                        if(readcosts>=curcosts){
                            System.out.println("hi");
                            readcosts=curcosts;
                            
                            for(Integer si : m.getValue()){
                                if(!checkInnerParts(parts, si)){
                                    sn.add(si);
                                    parts.set(counter, sn);
                                    curcosts=calcHPCostsReverse(conn, parts, store, s, idattr);


                                    if(cap>=curcosts){
                                        continue;
                                        
                                    }else{
                                        sn.remove(si);
                                        parts.set(counter, sn);
                                        break outerloop;
                                    }
                                }
                            }
                            
                        }else{
                            parts.remove(sn);
                            counter--;
                        }
                        
                    }
                                            
                }
                counter++;
            } 
        }
        return parts;
    }
    public static ArrayList<ArrayList<String>> createHortPartDuckDB(Connection conn, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String s)throws SQLException{
        long curcosts=0;
        int counter=0;
        int tupel=Tablestats.getNoTupel(conn, s);//muss später wo anders mit einzelnen Attributen aufgerufen werden 
        int tblsize=Tablestats.getNoAttr(conn, s);//funktioniert auch mit den aus VP erstellten Partitionen
        long cap=Tablestats.storageBudgetDuck(tblsize, tupel);     
        int counterAtt=0;
        int curcostsadd=0;
        ArrayList<ArrayList<String>> parts = new ArrayList<>();
        int max=0;        
        List<Entry<ArrayList<String>, Integer>> list = new ArrayList<Entry<ArrayList<String>, Integer>>();
        for(Map.Entry<ArrayList<String>, Integer> m : store.get(s).entrySet()){
            list.add(m);
        }
        list.sort((e1, e2) -> e2.getValue().compareTo(e1.getValue()));
        System.out.println(list);
        loop:
        for(Map.Entry<ArrayList<String>, Integer> al : list){
            parts.add(al.getKey());
            System.out.println(parts);
            curcosts=heuristic.calcDuckCosts(conn, parts, store, s);
            System.out.println("aktuelle Kosten:"+curcosts);
            System.out.println(cap);
            if(curcosts<cap){
                continue;
            }else {
                parts.remove(al.getKey());
            }
        }
        return parts;
    }
    public static int calcHPCosts( Connection conn, ArrayList<ArrayList<Integer>> parts, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String s, HashMap<Integer, String> idattr)throws SQLException{
        int curcosts=0;
        int numbq=0;
        int totalc=0;
        int tupelPart=0;
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select tablename from advisorvertpartstats where partname ='"+s+"'");
        String origintbl="";
        while(rs.next()){
            origintbl=rs.getString(1);
        }
        int tupel=Tablestats.getNoTupel(conn, origintbl);
        int tblsize=Tablestats.getNoAttr(conn, s);
        for(Map.Entry<ArrayList<String>, Integer> m : store.get(s).entrySet()){
            numbq+=m.getValue();
        }

        int counter=0;
        for(ArrayList<Integer> al : parts){
            ArrayList<String> cur = new ArrayList<>();
            int app=0;
            for(Integer i : al){
                counter++;
                for(Integer i2 : idattr.keySet()){
                    if(Integer.valueOf(i+1).equals(i2)){
                        cur.add(idattr.get(i2));

                    }
                }
            }
            int app2=0;
            for(Map.Entry<ArrayList<String>, Integer> m : store.get(s).entrySet()){
                if(cur.containsAll(m.getKey())){
                    app+=m.getValue();
                }else {
                    for(String s2 : m.getKey()){
                        if(cur.contains(s2)){
                            app2+=m.getValue();
                        }
                    }
                }
            }

            tupelPart=Tablestats.getNoTupelHorz(conn, origintbl, cur);
            curcosts+=(app * tblsize * tupelPart)+ (app2 * tblsize*(2*counter)*tupelPart);//hier spezifische Tupelanzhal ausrechnen abhängig von HP
            numbq=numbq-app-app2;
            app=0;
            app2=0;

        }
        
        totalc = curcosts + numbq * tblsize * tupel;
        return totalc;
    }

    public static int calcHPCostsReverse( Connection conn, ArrayList<ArrayList<Integer>> parts, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String s, HashMap<Integer, String> idattr)throws SQLException{
        int curcosts=0;
        int numbq=0;
        int totalc=0;
        int tupelPart=0;
        Statement st = conn.createStatement();
        
        int tupel=Tablestats.getNoTupel(conn, s);
        int tblsize=Tablestats.getNoAttr(conn, s);
        for(Map.Entry<ArrayList<String>, Integer> m : store.get(s).entrySet()){
            numbq+=m.getValue();
        }

        int counter=0;
        for(ArrayList<Integer> al : parts){
            ArrayList<String> cur = new ArrayList<>();
            int app=0;
            for(Integer i : al){
                counter++;
                for(Integer i2 : idattr.keySet()){
                    if(Integer.valueOf(i+1).equals(i2)){
                        cur.add(idattr.get(i2));

                    }
                }
            }
            int app2=0;
            for(Map.Entry<ArrayList<String>, Integer> m : store.get(s).entrySet()){
                if(cur.containsAll(m.getKey())){
                    app+=m.getValue();
                }else {
                    for(String s2 : m.getKey()){
                        if(cur.contains(s2)){
                            app2+=m.getValue();
                        }
                    }
                }
            }

            tupelPart=Tablestats.getNoTupelHorz(conn, s, cur);
            curcosts+=(app * tblsize * tupelPart)+ (app2 * tblsize*(2*counter)*tupelPart);//hier spezifische Tupelanzhal ausrechnen abhängig von HP
            numbq=numbq-app-app2;
            app=0;
            app2=0;

        }
        
        totalc = curcosts + numbq * tblsize * tupel;
        return totalc;
    }
    public static int calcDuckCosts( Connection conn, ArrayList<ArrayList<String>> parts, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String s)throws SQLException{
        int curcosts=0;
        int tupelPart=0;
        int tblsize=Tablestats.getNoAttr(conn, s);
        
        for(ArrayList<String> al : parts){

            tupelPart=Tablestats.getNoTupelHorz(conn, s, al);
            curcosts+=tupelPart*tblsize;
            
        }

        return curcosts;
    }

}
