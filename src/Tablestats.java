import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Tablestats {
    public static void main(String[] args){
        /*Diese Klasse dient dazu alle Randparameter (Anzahl Tupel, Anzahl erlaubte Attribute, Storage Cap... ) für die Kostenberechnung in Heuristics.java zu liefern*/
    }
    public static int getNoTupel(Connection conn, String tblname)throws SQLException{
        int number=0;
        Statement s = conn.createStatement();
        ResultSet rs = s.executeQuery("Select count(*) from "+tblname);
        while(rs.next()){
            number=rs.getInt(1);
        }
        return number;
    } 
    public static int getNoAttr(Connection conn, String tblname)throws SQLException{
        int number=0;
        Statement s = conn.createStatement();
        ResultSet rs = s.executeQuery("SELECT count(column_name) FROM information_schema.columns WHERE table_name='"+tblname+"'" );
        while(rs.next()){
            number=rs.getInt(1);
        }
        return number;
    }
    public static long storageBudget(int attrnumber, int tupnumber){
        double cap=attrnumber*tupnumber * 0.6;
        double zw = cap/tupnumber;
        long value= (long) zw;

        return value;
    }
    public static long storageBudgetDuck(int attrnumber, int tupnumber){
        double cap=attrnumber*tupnumber * 0.6;
        long capres = Math.round(cap);
        return capres;
    }

    public static int getNoTupelHorz(Connection conn, String tblname, ArrayList<String> bedingung)throws SQLException{
        int number=0;
        Statement s = conn.createStatement();
        
        if(bedingung.size()==1){
            String a=bedingung.get(0);
            String wert="";
            if(bedingung.get(0).contains("< ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf("< ")+1);
            }else if(bedingung.get(0).contains("<= ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf("<= ")+1);
            }else if(bedingung.get(0).contains("> ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf("> ")+1);
            }else if(bedingung.get(0).contains(">= ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf(">= ")+1);
            }else if(bedingung.get(0).contains(" = ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf(" = ")+1);
            }
            a=a.replaceAll(wert, "");

            ResultSet rs = s.executeQuery("Select count(*) from "+tblname+" where "+a+wert);
            while(rs.next()){
                number=rs.getInt(1);
            }
        } else{
            String a=bedingung.get(0);
            String wert="";
            if(bedingung.get(0).contains("< ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf("< ")+1);
            }else if(bedingung.get(0).contains("<= ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf("<= ")+1);
            }else if(bedingung.get(0).contains("> ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf("> ")+1);
            }else if(bedingung.get(0).contains(">= ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf(">= ")+1);
            }else if(bedingung.get(0).contains(" = ")){
                wert=bedingung.get(0).substring(bedingung.get(0).lastIndexOf("= ")+1);
            }
            a=a.replaceAll(wert, "");
            String nm=bedingung.get(1);
            String f = "Select count(*) from "+tblname+" where "+a+wert+" and "+nm;
            ResultSet rs = s.executeQuery(f);
            while(rs.next()){
                number=rs.getInt(1);
            }
        }
        return number;
    } 

}
