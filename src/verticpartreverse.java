import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class verticpartreverse {
    public static void main(String[] args, Connection conn) throws SQLException, IOException{
   
        HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store = new HashMap<>();
        store=appmatrix(conn);
        
        if(store.containsKey("")){
            store.remove("");
        }
        System.out.println(store);
        for(String s : store.keySet()){
            ArrayList<ArrayList<ArrayList<Integer>>> list = new ArrayList<>();
            HashMap<Integer, String> res = new HashMap<>();
            res=IDAttr(store, s);
            HashMap<ArrayList<String>, int[]> fstmatr=new HashMap<>();
            fstmatr=firstMatrix(res, store, s);
            int[][] matrix = new int[res.size()][res.size()];
            matrix=getFinalMatrix(res, fstmatr);

            ConcurrentHashMap<ArrayList<Integer>, Integer> maxkombis=new ConcurrentHashMap<>();
            maxkombis=mainPart(matrix);
            HashMap<Integer, ArrayList<Integer>> zuordnung =  new HashMap<>();
            ArrayList<Integer> cur = heuristic.getOrderMax(matrix);          
            ArrayList<Integer> reihenfolge=heuristic.getIDMax(matrix, cur);
            HashMap<Integer, ArrayList<Integer>> completeList=heuristic.innerMax(heuristic.getIDMax(matrix, cur), matrix);
            ArrayList<ArrayList<Integer>> parts =heuristic.createPartReverse(conn, store, reihenfolge, completeList, s, res);
            System.out.println(parts);
            ArrayList<String> namensliste = new ArrayList<>();
            int partcounter=0;
            for(ArrayList<Integer> al : parts){
                System.out.println(al);
                namensliste=getAttName(res, al);
                createVertPart(conn, s, namensliste, partcounter);
                partcounter++;
            }

        }
    }
    public static ArrayList<String> getAttName(HashMap<Integer, String> res, ArrayList<Integer> part){
        ArrayList<String> namensliste = new ArrayList<>();
        for(Integer i : part){
            for(Integer i2 : res.keySet()){
                if(Integer.valueOf(i+1).equals(i2)){
                    namensliste.add(res.get(i2));
                }
            }
        }
        return namensliste;
    }
    
    public static ConcurrentHashMap<String, Integer> countAttr(ConcurrentHashMap<String,Integer> list, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String s ){
        int counter=0;
        for(Map.Entry<String, Integer> l : list.entrySet()){
            for(Map.Entry<ArrayList<String>, Integer> a : store.get(s).entrySet()){
                if(a.getKey().contains(l.getKey())){
                    counter++;
                    list.put(l.getKey(), Integer.valueOf(counter));
                }
            }
            counter=0;
        }
        return list;
    }
    public static int maxAttr(ConcurrentHashMap<String, Integer> list){
        int max=0;
        for(Map.Entry<String, Integer> l2 : list.entrySet()){
            if(l2.getValue()>max){
                max=l2.getValue();
            }
        }
        return max;
    }
    
    public static String tablename(String s){
        String[] str = s.split("from ");
        for(String a : str){
            if(!a.contains("select")){
                if(a.contains("where")){
                    String[] cur = a.split(" where");
                    for(String f:cur){
                        if(!f.contains("where")){
                            return f;
                        }
                    }
                } else {
                    return a;
                }
            }
        }
        return "";
    }    

    
    //erstellt tatsächlich neue Tabellen
    public static void createVertPart(Connection conn, String s, ArrayList<String> res, int counter) throws SQLException{
        
    
        ArrayList<String> keylist = new ArrayList<>();
        String g ="select tblpkey from advisorstats where tablename=?";
        PreparedStatement pre = conn.prepareStatement(g);
        pre.setString(1, s);
        ResultSet rs = pre.executeQuery();
        String key="";
        while(rs.next()){
            key = rs.getString(1);
            if(!res.contains(key)){
                res.add(key);
            }
            keylist.add(key);
        }
        ArrayList<String> attlisteName = new ArrayList<>();
        ArrayList<String> attliste = new ArrayList<>();
        for(String a : res){
            String f ="select column_name, data_type, character_maximum_length from information_schema.columns where table_name=? and column_name=?";
            PreparedStatement pre2 = conn.prepareStatement(f);
            pre2.setString(1, s);
            pre2.setString(2, a);
            ResultSet daten = pre2.executeQuery();
            while(daten.next()){
                String resa = daten.getString(1);//attname
                String resb = daten.getString(2);//datentyp
                Integer leng = daten.getInt(3);//char anzahl
                attlisteName.add(resa);
                String att="";
                if(leng==0){
                    att = resa +" "+resb;
                }else{
                    att = resa +" "+resb+"("+leng+")";
                }
                
                attliste.add(att);
            }
        }
        
        System.out.println(attliste);
        String create = "";//alle Attribute + deren Datentypen hintereinander
        int counterZ=0;
        for(String att: attliste){
            if(attliste.size()>=counterZ){
                create+=att+",";
                counterZ++;
            } else {
                create+=att;
            }
        }
        //entfernt Daten
        
        String keycre="Primary Key(";//erstellt Primary Key bezeichnung in create table statement
        int counterP=0;
        if(keylist.size()>1){
            for(String h : keylist){
                counterP++;
                if(counterP==keylist.size()){
                    keycre+=h;
                } else {
                    keycre+=h+","; 
                }
            }
        } else{
            for(String h : keylist){
                keycre+=h;
            }
        }
        Statement ct = conn.createStatement();
        
        String partname = s+"v"+counter;//neuer Tabellenname für Partition
        
        for(String at : res){//advisorVertPartstats speichert tabellenname, partitionsname, attribut ab
            ct.executeUpdate("insert into advisorvertpartstats(tablename, partname, partattr) values( '"+s+"','"+partname+"','"+ at+"')");
        }
        String name ="";
        int counterN=1;
        for(String atname : attlisteName){
            if(attlisteName.size()>counterN){
                name+=atname+",";
                counterN++;
            }else {
                name+=atname;
            }
        }
        System.out.println(keycre);
        ct.executeUpdate("create table "+partname+"("+create+" "+keycre+"))");
        ct.executeUpdate("insert into "+partname+"("+name+") select "+name+" from "+s);
        ct.executeUpdate("insert into advisorstats (tablename, tblpkey) values( '"+partname+"', '"+key+"')");
        }
        public static HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> appmatrix(Connection conn)throws IOException, SQLException, NullPointerException{
            HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store = new HashMap<>(); 
            ConcurrentHashMap<ArrayList<String>, Integer> score = new ConcurrentHashMap<>();
            ArrayList<String> vert = new ArrayList<>();
            ArrayList<String> m = new ArrayList<>();
            int counter=1;
            int counterS=1;
            int counter2=1;
            String line="";
            BufferedReader br = new BufferedReader(new FileReader("anfragenhoriz.txt"));
            while(((line =br.readLine())!=null)){
                String name=tablename(line);
                vert = attwhere(name, line);
                ArrayList<String> lookup2=new ArrayList<>();
                String[] lookup = new String[vert.size()];
                String attrname="";
                String wert="";
                for(String c : vert){
                    if(c.contains(" < ")){
                        lookup=c.split("< ");
                        attrname=lookup[0];
                        wert=lookup[1];
                    } else if(c.contains(" <= ")){
                        lookup=c.split("<= ");
                        attrname=lookup[0];
                        wert=lookup[1];
                    } else if(c.contains(" >= ")){
                        lookup=c.split(">= ");
                        attrname=lookup[0];
                        wert=lookup[1];
                    } else if(c.contains(" > ")){
                        lookup=c.split("> ");
                        attrname=lookup[0];
                        wert=lookup[1];
                    } else if(c.contains(" = ")){
                        lookup=c.split(" = ");
                        attrname=lookup[0];
                        wert=lookup[1];
                        wert=wert.replace("'", "");
                        
                    }

                    attrname=attrname.replace(" ", "");
                    Statement s = conn.createStatement();
                    String q="select partname from advisorhorzpartstats where tablename='"+name+"' and attrname='"+attrname+"' and wert='"+wert+"'";
                    ResultSet rs = s.executeQuery(q);
                    String pname="";
                    while(rs.next()){
                        pname = rs.getString(1);
                        lookup2.add(pname);
                    }
                }
                HashMap<String, Integer> bestPart= new HashMap<>();
                for(String s2 : lookup2){
                    int freq=Collections.frequency(lookup2, s2);
                    bestPart.put(s2, freq);
                }
                int max=0;
                String bestpart="";
                for(Map.Entry<String, Integer> mp : bestPart.entrySet()){
                    if(mp.getValue()>max){
                        max=mp.getValue();
                        bestpart=mp.getKey();
                    }
                }
                
                if(store.containsKey(bestpart)){
                    line=line.replaceAll(",", "");
                    ArrayList<String> str = new ArrayList<>(Arrays.asList(line.split("where")));
                    for(String s : str){
                        if(s.contains("select")){
                            m = new ArrayList<>(Arrays.asList(s.split(" ")));
                            m.remove("select");
                            m.remove("from");
                            m.remove(name);
                        }
                    }

                    int counterSet=0;
                    for(String h : m){
                        if(h.contains("count")){
                            h=h.replace("count(", "");
                            h=h.replace(")", "");
                            m.set(counterSet, h);
                        }
                        counterSet++;
                    }

                    Set<ArrayList<String>> curr = store.get(bestpart).keySet();
                    if(curr.contains(m)){
                        counter2=store.get(bestpart).get(m)+1;
                        System.out.println(counter2);
                        store.get(bestpart).put(m, counter2);
                    }else{
                        store.get(bestpart).put(m, counter2);

                    }
                    counter2=1; 
                } else {
                    ConcurrentHashMap<ArrayList<String>, Integer> scoreNeu = new ConcurrentHashMap<>();
                    line=line.replaceAll(",", "");
                    ArrayList<String> res = new ArrayList<>();
                    ArrayList<String> str = new ArrayList<>(Arrays.asList(line.split("where")));
                    for(String s : str){
                        if(s.contains("select")){
                            m = new ArrayList<>(Arrays.asList(s.split(" ")));
                            m.remove("select");
                            m.remove("from");
                            m.remove(name);
                        }
                    }
                    int counterSet=0;
                    for(String h : m){
                        if(h.contains("count")){
                            h=h.replace("count(", "");
                            h=h.replace(")", "");
                            m.set(counterSet, h);
                        }
                        counterSet++;
                    }
                    for(String v : m){
                        res.add(v);
                    }
                    scoreNeu.put(res,counterS);
                    store.put(bestpart, scoreNeu);
                }             
            }
            return store;
        }
        public static ArrayList<String> attwhere(String name, String line){
            ArrayList<String> attr=new ArrayList<>();
            String[] str = line.split(name);
            System.out.println(str.length);
            for(String a : str){
                if(!a.contains("select")){
                    if(a.contains("where")){
                        String[] cur = a.split(" where ");
                        List<String> cur2 = new LinkedList<String>(Arrays.asList(cur));
                        cur2.remove(0);
                        for(String c: cur2){
                            if(c.contains(" and ")){
                                String[] curtwo= c.split(" and ");
                                for(String h : curtwo){
                                    if(!h.contains(" and ")){
                                        attr.add(h);
                                    }
                                }
                            } else{
                                attr.add(c);
                            } 
                        }
                    } 
                }
            }
            return attr;
        }
        public static HashMap<Integer, String> IDAttr(HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String tblname){
            int counterA=1;
            HashMap<Integer, String> speicher = new HashMap<>();
            for(ArrayList<String> a : store.get(tblname).keySet()){
                for(String d : a){
                    if(!speicher.containsValue(d)){
                        speicher.put(counterA, d);
                        counterA++;
                    }
                }
            }
            return speicher;
        }
        public static HashMap<ArrayList<String>, int[]> firstMatrix(HashMap<Integer, String> speicher, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String tblname){
            HashMap<ArrayList<String>, int[]> matrix = new HashMap<>();
            int i=0;
            for(ArrayList<String> a : store.get(tblname).keySet()){
                int [] appe = new int[speicher.size()];
                for(String y : speicher.values()){
                    if(a.contains(y)){
                        appe[i]=1*Integer.valueOf(store.get(tblname).get(a));

                    } else{
                        appe[i]=0;
                    }
                    i++;
                }
                i=0;
                matrix.put(a, appe);
            }
            return matrix;
        }
        public static int[][] getFinalMatrix(HashMap<Integer, String> speicher, HashMap<ArrayList<String>, int[]> matrix){
            int[][] PAMmatrix = new int[speicher.size()][speicher.size()];
            int nrRow=0;
            int nrLine=0;
            for(String s : speicher.values()){
                for(Map.Entry<Integer, String> a : speicher.entrySet()){
                    if(a.getValue().contains(s)){
                        nrRow=Integer.valueOf(a.getKey());
                    }
                }
                for(Map.Entry<ArrayList<String>,int[]> h : matrix.entrySet()){
                    if(h.getKey().contains(s)){
                        for(String b : h.getKey()){
                            for(Map.Entry<Integer, String> a2 : speicher.entrySet()){
                                if(a2.getValue().contains(b)){
                                    nrLine=Integer.valueOf(a2.getKey());
                                    int cur[]=matrix.get(h.getKey());
                                    PAMmatrix[nrRow-1][nrLine-1]+=cur[nrLine-1];
                                }
                            }
                        }
                    }
                } 
            }
            //System.out.println(Arrays.deepToString(PAMmatrix));
            return PAMmatrix;
        }
        
        public static ConcurrentHashMap<ArrayList<Integer>, Integer> mainPart(int[][] matrix){
            ConcurrentHashMap<ArrayList<Integer>, Integer> store = new ConcurrentHashMap<>();
            
            for(int i=0;i<matrix.length;i++){
                ArrayList<Integer> list = new ArrayList<>();
                int max=0;
                int vli=0;
                int vlj=0;
                for(int j=0;j<matrix.length;j++){
                    if(i!=j && matrix[i][j]>max){
                        max=matrix[i][j];
                        vli=i;
                        vlj=j;
                    }
                }
                list.add(vli);
                list.add(vlj);
                store.put(list, max);
            }
            return store;
        }
        
}
