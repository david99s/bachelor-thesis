import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class horzpartreverse {
    public static void main(String[] args, Connection conn) throws SQLException, IOException{
 
        HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store=new HashMap<>();
        store=appmatrix(conn);
        System.out.println(store);
        for(String s : store.keySet()){
            HashMap<Integer, String> res = new HashMap<>();
            res = IDAttr(store, s);
            HashMap<ArrayList<String>, int[]> matrix1 = new HashMap<>();
            matrix1=firstMatrix(res, store, s);
            int [][] curres = getFinalMatrix(res, matrix1);

            ArrayList<Integer> cur = heuristic.getOrderMax(curres);         
            ArrayList<Integer> reihenfolge=heuristic.getIDMax(curres, cur);
            HashMap<Integer, ArrayList<Integer>> completeList=heuristic.innerMax(heuristic.getIDMax(curres, cur), curres);
            ArrayList<ArrayList<String>> parts =heuristic.createHortPartDuckDB(conn, store, s);
            System.out.println(parts);
            
            int counter=1;
            for(ArrayList<String> al : parts){
                
                splitValue(al, conn, s, counter);
                counter++;
            }
        }
    }
    public static HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> appmatrix(Connection conn)throws IOException, SQLException, NullPointerException{
        HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store = new HashMap<>(); 
        ConcurrentHashMap<ArrayList<String>, Integer> score = new ConcurrentHashMap<>();
        ArrayList<String> attr = new ArrayList<>();
        ArrayList<String> vert = new ArrayList<>();
        int counterA=0;
        int counter=1;
        int counterS=1;
        String line="";
        BufferedReader br = new BufferedReader(new FileReader("anfragenhoriz.txt"));
        while(((line =br.readLine())!=null)){
            String name=(tablename(line));
            vert = getVertName(line);
            int counterIndex=0;
            System.out.println(vert);
            for(String ab : vert){
                counterIndex++;
                ab=ab.replace("count(", "");
                ab=ab.replace(")","");
                vert.set(counterIndex-1, ab);
            }
            attr=attwhere(name, line);
            if(store.containsKey(name)){
                Set<ArrayList<String>> curr = store.get(name).keySet();
                System.out.println(curr);
                if(curr.contains(attr)){
                    counter=store.get(name).get(attr)+1;
                    store.get(name).put(attr, counter);
                }else{
                    store.get(name).put(attr, counter);

                }
                counter=1;
            }else {
                ConcurrentHashMap<ArrayList<String>, Integer> scoreNeu = new ConcurrentHashMap<>();
                scoreNeu.put(attr,counterS);
                store.put(name, scoreNeu); 
            }
        }
        br.close();
        return store;
    }
    public static HashMap<Integer, String> IDAttr(HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String tblname){
        int counterA=1;
        HashMap<Integer, String> speicher = new HashMap<>();
        for(ArrayList<String> a : store.get(tblname).keySet()){
            for(String d : a){
                if(!speicher.containsValue(d)){
                    speicher.put(counterA, d);
                    counterA++;
                }
            }
        }
        return speicher;
    }

    public static HashMap<ArrayList<String>, int[]> firstMatrix(HashMap<Integer, String> speicher, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String tblname){
        HashMap<ArrayList<String>, int[]> matrix = new HashMap<>();
        int i=0;
        for(ArrayList<String> a : store.get(tblname).keySet()){
            int [] appe = new int[speicher.size()];
            for(String y : speicher.values()){
                if(a.contains(y)){
                    appe[i]=1*Integer.valueOf(store.get(tblname).get(a));

                } else{
                    appe[i]=0;
                }
                i++;
            }
            i=0;
            matrix.put(a, appe);
        }
        return matrix;
    }
    public static int[][] getFinalMatrix(HashMap<Integer, String> speicher, HashMap<ArrayList<String>, int[]> matrix){
        int nrRow=0;
        int nrLine=0;
        int[][] PAMmatrix = new int[speicher.size()][speicher.size()];
        for(String s : speicher.values()){
            for(Map.Entry<Integer, String> a : speicher.entrySet()){
                if(a.getValue().contains(s)){
                    nrRow=Integer.valueOf(a.getKey());
                }
            }
            for(Map.Entry<ArrayList<String>,int[]> h : matrix.entrySet()){
                if(h.getKey().contains(s)){
                    for(String b : h.getKey()){
                        for(Map.Entry<Integer, String> a2 : speicher.entrySet()){
                            if(a2.getValue().contains(b)){
                                nrLine=Integer.valueOf(a2.getKey());
                                int cur[]=matrix.get(h.getKey());
                                PAMmatrix[nrRow-1][nrLine-1]+=cur[nrLine-1];
                            }
                        }
                    }
                }
            } 
        }

        return PAMmatrix;
    }

    public static ArrayList<String> selectBestPart(int[][] PAMmatrix, HashMap<Integer, String> speicher){
        ArrayList<Integer> cur = findMax(PAMmatrix);
        ArrayList<String> res = new ArrayList<>();
        for(Integer i : cur){
            for(Map.Entry<Integer, String> s : speicher.entrySet()){
                if(s.getKey().equals(i)){
                    res.add(s.getValue());
                }
            }
        }
        return res;
    }
    public static ArrayList<String> createCounterPart(ArrayList<String> res, Connection conn, String tblname) throws SQLException{
        ArrayList<String> alt= new ArrayList<>();
        ArrayList<String> list= new ArrayList<>();
        Statement ct = conn.createStatement();
        
        for(String s : res){
            if(s.contains(" > ")){
                s=s.replaceAll(">", "<=");
                alt.add(s);
                
            } else if(s.contains(">=")){
                s=s.replaceAll(">=", "<");
                alt.add(s);
                
            }else if(s.contains(" < ")){
                s=s.replaceAll("<", ">=");
                System.out.println(s);
                alt.add(s);
                
            } else if(s.contains("<=")){
                s=s.replaceAll("<=", ">");
                alt.add(s);
                
            } else if(s.contains(" = ") && !s.contains("orders")){//Achtung hier sind somit nur = Abfragen bei Attribute die nur 2 versch. Werte besitzen, möglich
                String[] cur = s.split("=");
                String attr = cur[0];
                String val = cur[1];
                val=val.replaceAll("'", "");
                val=val.replaceFirst(" ", "");
                String q="Select "+attr+" from "+ tblname+" where "+attr+" not like '"+val+"' limit 1";
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(q);
                String valrp="";
                while(rs.next()){
                    valrp = rs.getString(1);
                    list.add(valrp);
                }
                s=s.replace(val, valrp);
                alt.add(s);
    
            }else if(s.contains(" = ") && s.contains("orders")){//Achtung hier sind somit nur = Abfragen bei Attribute die nur 2 versch. Werte besitzen, möglich
                String[] cur = s.split("=");
                String attr = cur[0];
                String val = cur[1];
                val=val.replaceAll("'", "");
                val=val.replaceFirst(" ", "");
                String q="Select distinct "+attr+" from "+ tblname+" where "+attr+" not like '"+val+"'";
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(q);
                String valrp="";
                while(rs.next()){
                    valrp = rs.getString(1);
                    list.add(valrp);
                }
                int counter=1;
                for(String rp : list){
                    if(counter==2){
                        s=s.replace("'"+list.get(0)+"'", "'"+rp+"'");
                    }else{
                        s=s.replace(val, rp);
                    }
                    alt.add(s);
                    counter++;
                }
            }
        }
        return alt;
    }
    public static void splitValue(ArrayList<String> best, Connection conn, String tblname, int counter)throws SQLException, IOException{
        //baut Statistiktabelle auf
        String q="";
        String partname = tblname+"h"+counter;
        String min="";
        String max="";
        
        Statement ct=conn.createStatement();
        
        for(String s : best){
            int counterCur=0;
            if(s.contains("<") || s.contains("<=")){
                String[] cursplit= new String[2];
                if(s.contains("<=")){
                    cursplit = s.split(" <=");
                }else{
                cursplit = s.split(" < ");
                }
                String attr=cursplit[0];
                for(String t : cursplit){
                    counterCur++;
                    if(counterCur==2){
                        String sd = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                        PreparedStatement ps = conn.prepareStatement(sd);
                        ps.setString(1, tblname);
                        ps.setString(2, partname);
                        ps.setString(3, attr);
                        t=t.replace(" ", "");
                        ps.setString(4, t);
                        ps.executeUpdate();
                    } else {
                        String q1="Select min("+attr+") from "+tblname;
                        Statement st = conn.createStatement();
                        ResultSet rs = st.executeQuery(q1);
                        while(rs.next()){
                            
                            min =rs.getString(1);
                        }
                        String sd = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                        PreparedStatement ps = conn.prepareStatement(sd);
                        ps.setString(1, tblname);
                        ps.setString(2, partname);
                        ps.setString(3, t);
                        ps.setString(4, min);
                        ps.executeUpdate();
                    } 
                } 
            } else if(s.contains(">") || s.contains(">=")){
                String[] cursplit= new String[2];
                if(s.contains(">=")){
                    cursplit = s.split(" >=");
                }else{
                cursplit = s.split(" >");
                }
                String attr=cursplit[0];
                for(String t : cursplit){
                    t=t.replace(" ","");
                    counterCur++;
                    if(counterCur==2){
                        String sd = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                        PreparedStatement ps = conn.prepareStatement(sd);
                        ps.setString(1, tblname);
                        ps.setString(2, partname);
                        ps.setString(3, attr);
                        ps.setString(4, t);
                        ps.executeUpdate();
                    } else {
                        String q1="Select max("+attr+") from "+tblname;
                        Statement st = conn.createStatement();
                        ResultSet rs = st.executeQuery(q1);
                        while(rs.next()){
                            max=rs.getString(1);
                            
                        }
                        String sd = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                        PreparedStatement ps = conn.prepareStatement(sd);
                        ps.setString(1, tblname);
                        ps.setString(2, partname);
                        ps.setString(3, t);
                        ps.setString(4, max);
                        ps.executeUpdate();
                    } 
                }
            
            } else if(s.contains(" = ")){
                String[] cur=s.split(" = ");
                String attr=cur[0];
                cur[1]=cur[1].replaceAll("'", "");
                String sd3 = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                PreparedStatement ps = conn.prepareStatement(sd3);
                ps.setString(1, tblname);
                ps.setString(2, partname);
                ps.setString(3, attr);
                ps.setString(4, cur[1]);
                ps.executeUpdate();
            }
            
        }
        
        createHort(best, conn, tblname, counter);
    }
    
    public static void createHort(ArrayList<String> ar, Connection conn, String tblname, int counter)throws SQLException, IOException{
        
        String g ="select tblpkey from advisorstats where tablename=?";
        ArrayList<String> keylist=new ArrayList<>();
        PreparedStatement pre = conn.prepareStatement(g);
        pre.setString(1, tblname);
        ResultSet rs = pre.executeQuery();
        String key="";
        while(rs.next()){
            key = rs.getString(1);
            keylist.add(key);
        }
        ArrayList<String> attlisteName = new ArrayList<>();
        ArrayList<String> attliste = new ArrayList<>();
        String f ="select column_name, data_type, character_maximum_length from information_schema.columns where table_name=?";
        PreparedStatement pre2 = conn.prepareStatement(f);
        pre2.setString(1, tblname);
        ResultSet daten = pre2.executeQuery();
        while(daten.next()){
            String resa = daten.getString(1);//attname
            String resb = daten.getString(2);//datentyp
            Integer leng = daten.getInt(3);//char anzahl
            attlisteName.add(resa);
            String att="";
            if(leng==0){
                att = resa +" "+resb;
            }else{
                att = resa +" "+resb+"("+leng+")";
            }
            
            attliste.add(att);
        }
        String create = "";//alle Attribute + deren Datentypen hintereinander
        int counterZ=0;
        for(String att: attliste){
            if(attliste.size()>=counterZ){
                create+=att+",";
                counterZ++;
            } else {
                create+=att;
            }
        }
        //entfernt Daten
        
        String keycre="Primary Key(";//erstellt Primary Key bezeichnung in create table statement
        int counterP=0;
        if(keylist.size()>1){
            for(String h : keylist){
                counterP++;
                if(counterP==keylist.size()){
                    keycre+=h;
                } else {
                    keycre+=h+","; 
                }
            }
        } else{
            for(String h : keylist){
                keycre+=h;
            }
        }
        String name ="";
        int counterN=1;
        for(String atname : attlisteName){
            if(attlisteName.size()>counterN){
                name+=atname+",";
                counterN++;
            }else {
                name+=atname;
            }
        }
        if(ar.size()>0){
            String a=ar.get(0);
            String wert="";
            if(ar.contains(" < ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf("< ")+1);
                a=a.replaceAll(wert, "");
            } else if(ar.contains(" <= ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf("<= ")+1);
                a=a.replaceAll(wert, "");
            } else if(ar.contains(" >= ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf(">= ")+1);
                a=a.replaceAll(wert, "");
            } else if(ar.contains(" > ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf("> ")+1);
                a=a.replaceAll(wert, "");
            } else if(ar.contains(" = ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf("=")+1);

                a=a.replaceAll(wert, "");
            }
            if(ar.size()>1){
 
                String s=ar.get(1);
                String[] attr2=new String[2];
                String bsp="";
                String partname=tblname+"h"+counter;
                if(s.contains("<")){
                    attr2=s.split("<");
                    bsp="insert into "+partname+"("+name+") select "+name+" from "+tblname+" where "+a+wert+" and "+attr2[0]+"<"+attr2[1];
                }else {
                    bsp="insert into "+partname+"("+name+") select "+name+" from "+tblname+" where "+a+wert+" and "+s;
                }
                Statement ct = conn.createStatement();
            
                ct.executeUpdate("create table "+partname+"("+create+" "+keycre+"))");
                counter++;

                ct.executeUpdate(bsp);

                ct.executeUpdate("insert into advisorstats (tablename, tblpkey) values( '"+partname+"', '"+key+"')");
            
                
            }else{
                Statement ct = conn.createStatement();
                String partname=tblname+"h"+counter;
                ct.executeUpdate("create table "+partname+"("+create+" "+keycre+"))");
                counter++;

                String bsp = "insert into "+partname+"("+name+") select "+name+" from "+tblname+" where "+a+wert;

                ct.executeUpdate(bsp);
                ct.executeUpdate("insert into advisorstats (tablename, tblpkey) values( '"+partname+"', '"+key+"')");
            } 
        }
    }
    public static ArrayList<Integer> findMax(int[][] PAMMatrix){
        int max=0;
        int attr1=0;
        int attr2=0;
        ArrayList<Integer> res = new ArrayList<>();
        for(int i=0;i<PAMMatrix.length;i++){
            for(int j=0;j<PAMMatrix[i].length;j++){
                if(max < PAMMatrix[i][j] && i != j){
                    max=PAMMatrix[i][j];
                    attr1=i+1;
                    attr2=j+1;
                }
            }
            
        }
        res.add(attr1);
        res.add(attr2);
        System.out.println(res);
        return res;
    }

    public static String tablename(String s){
        String[] str = s.split("from ");
        for(String a : str){
            if(!a.contains("select")){
                if(a.contains("where")){
                    String[] cur = a.split(" where");
                    for(String f:cur){
                        if(!f.contains("where")){
                            return f;
                        }
                    }
                } else {
                    return a;
                }
            }
        }
        return "";
    }
    public static ArrayList<String> getVertName(String s){
        String[] str = s.split("from ");
        ArrayList<String> attr = new ArrayList<>();
        for(String a : str){
            if(!a.contains("select")){
                continue;
            } else if(a.contains("select")){
                String[] ar = a.split(" ");
                for(String h : ar){
                    if(!h.contains("select")){
                        String[] ar2 = h.split(",");
                        for(String k : ar2){
                            attr.add(k);
                        }
                        
                    }
                }
            }
        }
        return attr;
    }
    public static ArrayList<String> attwhere(String name, String line){
        ArrayList<String> attr=new ArrayList<>();
        String[] str = line.split(name,2);
        System.out.println(str.length);
        for(String a : str){
            if(!a.contains("select")){
                if(a.contains("where")){
                    String[] cur = a.split(" where ");
                    List<String> cur2 = new LinkedList<String>(Arrays.asList(cur));
                    
                    cur2.remove(0);
                    for(String c: cur2){
                        if(c.contains(" and ")){
                            String[] curtwo= c.split(" and ");
                            for(String h : curtwo){
                                if(!h.contains(" and ")){
                                    attr.add(h);
                                }
                            }
                        } else{
                            //System.out.println(c);
                            attr.add(c);
                        } 
                    }
                } 
            }
        }
        return attr;
    }
    public static ArrayList<String> getAttName(HashMap<Integer, String> res, ArrayList<Integer> part){
        ArrayList<String> namensliste = new ArrayList<>();
        for(Integer i : part){
            for(Integer i2 : res.keySet()){
                if(Integer.valueOf(i+1).equals(i2)){
                    namensliste.add(res.get(i2));
                }
            }
        }
        return namensliste;
    }
}
