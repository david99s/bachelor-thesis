import java.sql.Statement;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class verticalpart {
    
    public static void main(String[] args, Connection conn) throws SQLException, IOException{

        HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store = new HashMap<>();
        store=appmatrix();//summiert pro Tabelle die Attributkombinationen
        
        if(store.containsKey("")){
            store.remove("");
        }
        System.out.println(store);
        for(String s : store.keySet()){
            ArrayList<ArrayList<ArrayList<Integer>>> list = new ArrayList<>();
            HashMap<Integer, String> res = new HashMap<>();
            res=IDAttr(store, s);
            HashMap<ArrayList<String>, int[]> fstmatr=new HashMap<>();
            fstmatr=firstMatrix(res, store, s);
            int[][] matrix = new int[res.size()][res.size()];
            matrix=getFinalMatrix(res, fstmatr);//AA-Matrix
            ArrayList<Integer> cur = heuristic.getOrderMax(matrix);   
            ArrayList<Integer> reihenfolge=heuristic.getIDMax(matrix, cur);//Gibt Reihenfolge an in der die Attribute mit den besten AA-values dran sind
            HashMap<Integer, ArrayList<Integer>> completeList=heuristic.innerMax(heuristic.getIDMax(matrix, cur), matrix);
            ArrayList<ArrayList<Integer>> parts =heuristic.createPart(conn, store, reihenfolge, completeList, s, res);
            System.out.println(parts);
            ArrayList<String> namensliste = new ArrayList<>();
            int partcounter=0;
            for(ArrayList<Integer> al : parts){
                System.out.println(al);
                namensliste=getAttName(res, al);
                createVertPart(conn, s, namensliste, partcounter);//erstellt VP
                partcounter++;
            }

        }
    }
    public static ArrayList<String> getAttName(HashMap<Integer, String> res, ArrayList<Integer> part){
        ArrayList<String> namensliste = new ArrayList<>();
        for(Integer i : part){
            for(Integer i2 : res.keySet()){
                if(Integer.valueOf(i+1).equals(i2)){
                    namensliste.add(res.get(i2));
                }
            }
        }
        return namensliste;
    }
    
    public static ConcurrentHashMap<String, Integer> countAttr(ConcurrentHashMap<String,Integer> list, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String s ){
        int counter=0;
        for(Map.Entry<String, Integer> l : list.entrySet()){
            for(Map.Entry<ArrayList<String>, Integer> a : store.get(s).entrySet()){
                if(a.getKey().contains(l.getKey())){
                    counter++;
                    list.put(l.getKey(), Integer.valueOf(counter));
                }
            }
            counter=0;
        }
        return list;
    }
    public static int maxAttr(ConcurrentHashMap<String, Integer> list){
        int max=0;
        for(Map.Entry<String, Integer> l2 : list.entrySet()){
            if(l2.getValue()>max){
                max=l2.getValue();
            }
        }
        return max;
    }
    
    public static String tablename(String s){
        String[] str = s.split("from ");
        for(String a : str){
            if(!a.contains("select")){
                if(a.contains("where")){
                    String[] cur = a.split(" where");
                    for(String f:cur){
                        if(!f.contains("where")){
                            return f;
                        }
                    }
                } else {
                    return a;
                }
            }
        }
        return "";
    }    

    
    //erstellt tatsächlich neue Tabellen
    public static void createVertPart(Connection conn, String s, ArrayList<String> res, int counter) throws SQLException{
        
    
        ArrayList<String> keylist = new ArrayList<>();
        String g ="select tblpkey from advisorstats where tablename=?";
        PreparedStatement pre = conn.prepareStatement(g);
        pre.setString(1, s);
        ResultSet rs = pre.executeQuery();
        String key="";
        while(rs.next()){
            key = rs.getString(1);
            if(!res.contains(key)){
                res.add(key);
            }
            keylist.add(key);
        }
        ArrayList<String> attlisteName = new ArrayList<>();
        ArrayList<String> attliste = new ArrayList<>();
        for(String a : res){
            String f ="select column_name, data_type, character_maximum_length from information_schema.columns where table_name=? and column_name=?";
            PreparedStatement pre2 = conn.prepareStatement(f);
            pre2.setString(1, s);
            pre2.setString(2, a);
            ResultSet daten = pre2.executeQuery();
            while(daten.next()){
                String resa = daten.getString(1);//attname
                String resb = daten.getString(2);//datentyp
                Integer leng = daten.getInt(3);//char anzahl
                attlisteName.add(resa);
                String att="";
                if(leng==0){
                    att = resa +" "+resb;
                }else{
                    att = resa +" "+resb+"("+leng+")";
                }
                
                attliste.add(att);
            }
        }
        
        System.out.println(attliste);
        String create = "";//alle Attribute + deren Datentypen hintereinander
        int counterZ=0;
        for(String att: attliste){
            if(attliste.size()>=counterZ){
                create+=att+",";
                counterZ++;
            } else {
                create+=att;
            }
        }
        //entfernt Daten
        
        String keycre="Primary Key(";//erstellt Primary Key bezeichnung in create table statement
        int counterP=0;
        if(keylist.size()>1){
            for(String h : keylist){
                counterP++;
                if(counterP==keylist.size()){
                    keycre+=h;
                } else {
                    keycre+=h+","; 
                }
            }
        } else{
            for(String h : keylist){
                keycre+=h;
            }
        }
        Statement ct = conn.createStatement();
        
        String partname = s+counter;//neuer Tabellenname für Partition
        
        for(String at : res){//advisorVertPartstats speichert tabellenname, partitionsname, attribut ab
            ct.executeUpdate("insert into advisorvertpartstats(tablename, partname, partattr) values( '"+s+"','"+partname+"','"+ at+"')");
        }
        String name ="";
        int counterN=1;
        for(String atname : attlisteName){
            if(attlisteName.size()>counterN){
                name+=atname+",";
                counterN++;
            }else {
                name+=atname;
            }
        }
        System.out.println(keycre);
        ct.executeUpdate("create table "+partname+"("+create+" "+keycre+"))");
        ct.executeUpdate("insert into "+partname+"("+name+") select "+name+" from "+s);
        ct.executeUpdate("insert into advisorstats (tablename, tblpkey) values( '"+partname+"', '"+key+"')");
        }
        public static HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> appmatrix()throws IOException, SQLException, NullPointerException{
            HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store = new HashMap<>(); 
            ConcurrentHashMap<ArrayList<String>, Integer> score = new ConcurrentHashMap<>();
            
            ArrayList<String> m = new ArrayList<>();
            int counter=1;
            int counterS=1;
            int counter2=1;
            String line="";
            BufferedReader br = new BufferedReader(new FileReader("anfragenhoriz.txt"));
            while(((line =br.readLine())!=null)){
                String name=tablename(line);
                if(store.containsKey(name)){
                    line=line.replaceAll(",", "");
                    ArrayList<String> str = new ArrayList<>(Arrays.asList(line.split("where")));
                    for(String s : str){
                        if(s.contains("select")){
                            m = new ArrayList<>(Arrays.asList(s.split(" ")));
                            m.remove("select");
                            m.remove("from");
                            m.remove(name);
                        }
                    }

                    int counterSet=0;
                    for(String h : m){
                        if(h.contains("count")){
                            h=h.replace("count(", "");
                            h=h.replace(")", "");
                            m.set(counterSet, h);
                        }
                        counterSet++;
                    }

                    //attr=attwhere(name, line);
                    Set<ArrayList<String>> curr = store.get(name).keySet();
                    if(curr.contains(m)){
                        counter2=store.get(name).get(m)+1;
                        store.get(name).put(m, counter2);
                    }else{
                        store.get(name).put(m, counter2);
                    }
                    counter2=1; 
                } else {
                    ConcurrentHashMap<ArrayList<String>, Integer> scoreNeu = new ConcurrentHashMap<>();
                    line=line.replaceAll(",", "");
                    ArrayList<String> res = new ArrayList<>();
                    ArrayList<String> str = new ArrayList<>(Arrays.asList(line.split("where")));
                    for(String s : str){
                        if(s.contains("select")){
                            m = new ArrayList<>(Arrays.asList(s.split(" ")));
                            m.remove("select");
                            m.remove("from");
                            m.remove(name);
                        }
                    }
                    int counterSet=0;
                    for(String h : m){
                        if(h.contains("count")){
                            h=h.replace("count(", "");
                            h=h.replace(")", "");
                            System.out.println(h);
                            m.set(counterSet, h);
                        }
                        counterSet++;
                    }
                    for(String v : m){
                        res.add(v);
                    }
                    scoreNeu.put(res,counterS);
                    store.put(name, scoreNeu);
                }             
            }
            return store;
        }
        public static HashMap<Integer, String> IDAttr(HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String tblname){
            int counterA=1;
            HashMap<Integer, String> speicher = new HashMap<>();
            for(ArrayList<String> a : store.get(tblname).keySet()){
                for(String d : a){
                    if(!speicher.containsValue(d)){
                        speicher.put(counterA, d);
                        counterA++;
                    }
                }
            }
            return speicher;
        }
        public static HashMap<ArrayList<String>, int[]> firstMatrix(HashMap<Integer, String> speicher, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String tblname){
            HashMap<ArrayList<String>, int[]> matrix = new HashMap<>();
            int i=0;
            for(ArrayList<String> a : store.get(tblname).keySet()){
                int [] appe = new int[speicher.size()];
                for(String y : speicher.values()){
                    if(a.contains(y)){
                        appe[i]=1*Integer.valueOf(store.get(tblname).get(a));
                        
                    } else{
                        appe[i]=0;
                    }
                    i++;
                }
                i=0;
                matrix.put(a, appe);
            }
            return matrix;
        }
        public static int[][] getFinalMatrix(HashMap<Integer, String> speicher, HashMap<ArrayList<String>, int[]> matrix){// zum implizieren von Attributen, wenn ein Attribut das andere impliziert bekommt es dessen Punkte auch
            int[][] PAMmatrix = new int[speicher.size()][speicher.size()];
            int nrRow=0;
            int nrLine=0;
            for(String s : speicher.values()){
                for(Map.Entry<Integer, String> a : speicher.entrySet()){
                    if(a.getValue().contains(s)){
                        nrRow=Integer.valueOf(a.getKey());
                    }
                }
                for(Map.Entry<ArrayList<String>,int[]> h : matrix.entrySet()){
                    if(h.getKey().contains(s)){
                        for(String b : h.getKey()){
                            for(Map.Entry<Integer, String> a2 : speicher.entrySet()){
                                if(a2.getValue().contains(b)){
                                    nrLine=Integer.valueOf(a2.getKey());
                                    int cur[]=matrix.get(h.getKey());
                                    PAMmatrix[nrRow-1][nrLine-1]+=cur[nrLine-1];
                                }
                            }
                        }
                    }
                } 
            }
            return PAMmatrix;
        }
        
        public static ConcurrentHashMap<ArrayList<Integer>, Integer> mainPart(int[][] matrix){
            ConcurrentHashMap<ArrayList<Integer>, Integer> store = new ConcurrentHashMap<>();
            
            for(int i=0;i<matrix.length;i++){
                ArrayList<Integer> list = new ArrayList<>();
                int max=0;
                int vli=0;
                int vlj=0;
                for(int j=0;j<matrix.length;j++){
                    if(i!=j && matrix[i][j]>max){
                        max=matrix[i][j];
                        vli=i;
                        vlj=j;

                    }
                }
                list.add(vli);
                list.add(vlj);
                store.put(list, max);
            }
            return store;
        }
        
}
