import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.security.KeyStore.Entry;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import javax.lang.model.util.ElementScanner7;

//import org.postgresql.core.SqlCommand; 



public class Advisor {

    public static void main(String[] args) throws FileNotFoundException, IOException, SQLException, ParseException {
        /* 
        Grundsätzlicher Aufbau: Gewünschte Datenbank wird eingegeben. Dann wird das hinterlegte Datenbankverbindungsfile eingelesen. 
        Die Verbindungsfiles connectionPSQL.txt und connectionDuck.txt müssen entsprechend der Verbindung auf dem jeweiligen auszuführenden PC verändert werden
        In der Klasse Tabelstats muss zudem in der Methode storageBudget() das gewünschte Budget in Dezimalzahl angegeben werden.
        Je nach Datenbanktyp werden dann die entsprechenden Klassen für die Partitionierungen aufgerufen. 
        Nachdem die Partitionen erstellt worden sind, kann dann die entsprechende Executionmethode (VP-HP, HP-VP mit redundanz, HP für DuckDB mit redundanz) aufgerufen werden.
        In den execution Methoden kann dann entweder die anfragenhoriz.txt für den gesamten Workload oder anfrageneinzeln.txt für einzelne Queries aufgerufen werden*/
        Connection conn=null;
        Connection conn2=null;
        Scanner input = new Scanner(System.in);
        System.out.println("Which Database would you like to access: ");
        String db = input.next();
        if(db.equals("PostgreSQL")){
            conn =connStartpg(conn);
            verticalpart.main(args, conn);
            horizpart.main(args, conn);
        } else if(db.equals("PSQLreverse")){
            conn =connStartpg(conn);
            horzpartreverse.main(args, conn);
            verticpartreverse.main(args, conn);
        } else if(db.equals("DuckDB")){
            conn2 = connStartDuck(conn);
            horzpartDuckDb.main(args, conn);
        }
              
        
        //executeQueries(conn);
        //executeQueriesDuck(conn2);
        //executeQueriesReverse(conn);
    }
    //Verbindungsaufbau zu PostgreSQL
    public static Connection connStartpg(Connection conn) throws FileNotFoundException, IOException{
        
        try{BufferedReader lr = new BufferedReader(new FileReader("connectionPSQL.txt"));
            ArrayList<String> info = new ArrayList<>();
            String store="";
            while((store=lr.readLine())!=null){
                info.add(store);
            }
            conn = DriverManager.getConnection(info.get(0), info.get(1), info.get(2));
            if(conn!=null){
                System.out.println("Connection to PostgreSQL established");
            }
            lr.close();
            return conn;
        } catch (SQLException e){
            e.printStackTrace();
        }
        return conn;
    }
    //Verbindungsaufbau zu DuckDB
    public static Connection connStartDuck(Connection conn ) throws FileNotFoundException, IOException{
        
        try{BufferedReader lr = new BufferedReader(new FileReader("connectionDuck.txt"));
            ArrayList<String> info = new ArrayList<>();
            String store="";
            while((store=lr.readLine())!=null){
                info.add(store);
            }
            conn = DriverManager.getConnection(info.get(0));
            if(conn!=null){
                System.out.println("Connection to DuckDB established");
            }
            lr.close();
            return conn;
        } catch (SQLException e){
            e.printStackTrace();
        }
        return conn;
    }
    
    //Ausführung des Workloads für Variante VP-HP, es wird zuerst die passende VP gesucht und dafür alle HPs durchgegangen und geschaut welche am besten zur Anfrage passt.
    public static void executeQueries(Connection conn) throws FileNotFoundException, SQLException, IOException, ParseException{
        conn.setAutoCommit(false);  
        int counter=0;
        int completeTime=0;
        String result="";
        try {BufferedReader br = new BufferedReader(new FileReader("anfrageneinzeln.txt"));
            String sline="";

            ArrayList<String> vert = new ArrayList<>();
            int counterAus=1;
            while((sline=br.readLine()) != null){
                counter++;
                HashMap<String, Integer> curList = new HashMap<>();
                HashMap<Integer, HashMap<String, ArrayList<String>>> speicher = new HashMap<>();
                String name=(horizpart.tablename(sline));//Tabellennamen
                vert = horizpart.getVertName(sline);//Name der VP
                int counterIndex=0;
                for(String ab : vert){
                    counterIndex++;
                    ab=ab.replace("count(", "");
                    ab=ab.replace(")","");
                    vert.set(counterIndex-1, ab);
                }
                for(String a : vert){
                    String f = "select partname from advisorvertpartstats where tablename=? and partattr=?";//Suche nach VP-Namen
                    PreparedStatement s = conn.prepareStatement(f);
                    s.setString(1, name);
                    s.setString(2, a);
                    ResultSet rs = s.executeQuery();
                    String pname="";
                    
                    while(rs.next()){
                        pname=rs.getString(1);
                        if(curList.containsKey(pname)){
                            curList.put(pname, curList.get(pname)+1);
                        } else {
                            curList.put(pname, 1);
                        }
                    }
                    
                    String f2 = "select tblpkey from advisorstats where tablename=?";
                    PreparedStatement s2 = conn.prepareStatement(f2);
                    s2.setString(1, pname);
                    ResultSet rs2 = s2.executeQuery();
                    String pkey="";
                    while(rs2.next()){
                        pkey=rs2.getString(1);
                    }
                }
                
                String newtbl ="";
                if(vert.size()==3){
                    for(String s : curList.keySet()){
                        if(curList.get(s)>2){
                            newtbl=s;
                        }
                    }
                } else{
                    for(String s : curList.keySet()){
                        if(curList.get(s)>1){
                            newtbl=s;
                        }
                    }
                }
                System.out.println(newtbl);//Namen der entsprechenden VP
                
                if(!newtbl.isEmpty()){
                    ArrayList<String> attribute = new ArrayList<>();
                    attribute=horizpart.attwhere(name, sline);
                    System.out.println(attribute);
                    String[] cursplit=new String[2];
                    HashMap<String, String[]> operator=new HashMap<>();
                    int counterLe=0;
                    int counterL=0;
                    int counterGe=0;
                    int counterG=0;
                    int counterE=0;
                    ArrayList<String[]> abfragen = new ArrayList<>();
                    for(String s : attribute){
                        if(s.contains("<") || s.contains("<=")){
                            if(operator.containsKey(" < ")||operator.containsKey("<=")){
                                cursplit= new String[2];
                                if(s.contains("<=")){
                                    cursplit = s.split(" <=");
                                    operator.put("<= "+counterLe, cursplit);
                                    counterLe++;
                                }else{
                                cursplit = s.split(" < ");
                                operator.put("< "+counterL, cursplit);
                                counterL++;
                                }
                                
                            } else{
                                cursplit= new String[2];
                                if(s.contains("<=")){
                                    cursplit = s.split(" <=");
                                    operator.put("<=", cursplit);
                                    
                                }else{
                                cursplit = s.split(" < ");
                                operator.put("<", cursplit);
                                }
                            }
                            
                        } else if(s.contains(">") || s.contains(">=")){
                            if(operator.containsKey(" > ")||operator.containsKey(">=")){
                                cursplit= new String[2];
                                if(s.contains(">=")){
                                    cursplit = s.split(" >=");
                                    operator.put(">= "+counterGe, cursplit);
                                    counterGe++;
                                }else{
                                cursplit = s.split(" >");
                                operator.put("> "+counterG, cursplit);
                                counterG++;
                                }
                            }else{
                                cursplit= new String[2];
                                if(s.contains(">=")){
                                    cursplit = s.split(" >=");
                                    operator.put(">= ", cursplit);
                                }else{
                                cursplit = s.split(" >");
                                operator.put("> ", cursplit);
                                }
                            }
                        } else if(s.contains(" = ")){
                            if(operator.containsKey(" = ")){
                                cursplit=s.split(" = ");
                                operator.put("="+counterE, cursplit);
                                counterE++;
                            } else{
                                cursplit=s.split(" = ");
                                System.out.println(cursplit[0]);
                                operator.put("=", cursplit);
                            }
                        }
                        String getH = "select partname from advisorhorzpartstats where tablename='"+newtbl+"' and attrname='"+cursplit[0]+"'";// and where attrname="+attname);
                        Statement s3 = conn.createStatement();
                        ResultSet rs = s3.executeQuery(getH);
                        HashMap<String, ArrayList<String>> sm = new HashMap<>();
                        ArrayList<String> cur2 = new ArrayList<>();
                        String cur="";
                        while(rs.next()){
                            String nm = rs.getString(1);
                            cur2.add(nm);
                        }
                        
                        
                        for(String i : cur2){
                            String getZ = "select werte from advisorhorzpartstats where tablename='"+newtbl+"' and attrname='"+cursplit[0]+"' and partname='"+i+"'";// and where attrname="+attname);
                            Statement s4 = conn.createStatement();
                            ResultSet r4 = s4.executeQuery(getZ);
                            ArrayList<String> werte = new ArrayList<>();
                            while(r4.next()){
                                werte.add(r4.getString(1));
                            }
                            sm.put(i, werte);
                        }
                        abfragen.add(cursplit);
                        speicher.put(counter, sm);
                        counter++;
                    } 
                    
                    System.out.println(speicher);//speicher enthält für jedes attribut aus anfrage mögliche partitionen
                    
                    if(speicher.size()==2 && speicher.get(2)==null){
                        newtbl="";
                        attribute=null;
                    }
                    ArrayList<String> partwahl= new ArrayList<>();
                    if(attribute.isEmpty()){
                        String getH = "select distinct partname from advisorhorzpartstats where tablename='"+newtbl+"'";
                        
                        Statement s3 = conn.createStatement();
                        ResultSet rs = s3.executeQuery(getH);
                        HashMap<String, ArrayList<String>> sm = new HashMap<>();
                        ArrayList<String> cur2 = new ArrayList<>();
                        while(rs.next()){
                            String nm = rs.getString(1);
                            cur2.add(nm);
                            System.out.println(nm);   
                        }
                        String cur="";
                        int countercur=0;
                        System.out.println(cur2.size());
                        for(String scur : cur2){
                            countercur++;
                            String curS="Select count("+vert.get(0)+"), count("+vert.get(1)+") from ";
                            if(countercur==cur2.size()){
                                cur+=curS+scur;
                            } else{
                                cur+=curS+scur+" union all ";
                            }
                        }
                        System.out.println(cur);
                        result=cur;
                    } else{//Suche nach richtiger HP
                        for(String s : attribute){
                            if(s.contains(" > ")){
                                int counterEin=0;
                                for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                                    counterEin++;
                                    if(counterEin==2){
                                        for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                            int index=s.lastIndexOf(" > ");
                                            index+=3;
                                            
                                            String wert=s.substring(index);
                                            
                                            
                                            boolean zeiger=true;
                                            for(String zt : ch.getValue()){
                                                
                                                if(zt.contains(wert)){
                                                    zeiger=false;
                                                }
                                                if(zeiger){
                                                    partwahl.add(ch.getKey());
                                                } 
                                            }
                                        }
                                    } else if(counterEin==1){
                                        for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                            int index=s.lastIndexOf(" > ");
                                            index+=3;
                                            
                                            String wert=s.substring(index);
                                            System.out.println(wert);
                                            int counterW=0;
                                            boolean zeiger=true;
                                            ArrayList<String> cmp=ch.getValue();
                                            String w=cmp.get(0).replaceAll(" ","");
                                            w=w.replace(".", "");
                                            String w2=cmp.get(1).replaceAll(" ","");
                                            w2=w2.replace(".", "");
                                            
                                            if((Integer.parseInt(wert)<=Integer.parseInt(w) && Integer.parseInt(wert)<=Integer.parseInt(w2)) || (Integer.parseInt(w)>=Integer.parseInt(wert) && Integer.parseInt(wert)>=Integer.parseInt(w2))){
                                                partwahl.add(ch.getKey());
                                            }
                                        }
                                    }
                                }
                            } else if(s.contains(">=")){
                                int counterEin=0;
                                for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                                    counterEin++;
                                    if(counterEin==2){
                                        for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                            int index=s.lastIndexOf(" >= ");
                                            index+=3;
                                            
                                            String wert=s.substring(index);
                                            
                                            
                                            boolean zeiger=true;
                                            for(String zt : ch.getValue()){
                                                
                                                if(zt.contains(wert)){
                                                    zeiger=false;
                                                }
                                                if(zeiger){
                                                    partwahl.add(ch.getKey());
                                                } 
                                            }
                                        }
                                    } else if(counterEin==1){
                                        for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                            int index=s.lastIndexOf(" >= ");
                                            index+=3;
                                            
                                            String wert=s.substring(index);
                                            
                                            wert=wert.replace(" ", "");
                                            int counterW=0;
                                            boolean zeiger=true;
                                            ArrayList<String> cmp=ch.getValue();
                                            String w=cmp.get(0).replaceAll(" ","");
                                            w=w.replace(".", "");
                                            String w2=cmp.get(1).replaceAll(" ","");
                                            w2=w2.replace(".", "");
                                            //wert=wert.replaceAll(".","");
                                            if(Integer.parseInt(wert)<=Integer.parseInt(w) && Integer.parseInt(wert)<=Integer.parseInt(w2)){
                                                partwahl.add(ch.getKey());
                                            }
                                        }
                                    }
                                }
                            } else if(s.contains("<=")){
                                int counterEin=0;
                                for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                                    counterEin++;
                                    if(counterEin==2){
                                        for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                            int index=s.lastIndexOf(" <= ");
                                            index+=3;
                                            
                                            String wert=s.substring(index);

                                            boolean zeiger=true;
                                            for(String zt : ch.getValue()){
                                                
                                                if(zt.contains(wert)){
                                                    zeiger=false;
                                                }
                                                if(zeiger){
                                                    partwahl.add(ch.getKey());
                                                } 
                                            }
                                        }
                                    } else if(counterEin==1){
                                        for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                            int index=s.lastIndexOf(" <= ");
                                            index+=3;
                                            
                                            String wert=s.substring(index);
                                            
                                            int counterW=0;
                                            boolean zeiger=true;
                                            ArrayList<String> cmp=ch.getValue();
                                            if((Integer.parseInt(cmp.get(0))<=Integer.parseInt(wert) && Integer.parseInt(wert)<=Integer.parseInt(cmp.get(1)) || (Integer.parseInt(cmp.get(0))>=Integer.parseInt(wert) && Integer.parseInt(wert)>=Integer.parseInt(cmp.get(1))))){
                                                partwahl.add(ch.getKey());
                                            }
                                        }
                                    }
                                }
                            } else if(s.contains(" < ")){
                                int counterEin=0;
                                for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                                    counterEin++;
                                    if(counterEin==2){
                                        for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                            int index=s.lastIndexOf(" < ");
                                            index+=3;
                                            String wert=s.substring(index);
                                            int counterW=0;
                                            boolean zeiger=true;
                                            for(String zt : ch.getValue()){
                                                counterW++;
                                                if(zt.contains(wert)){
                                                    zeiger=false;
                                                }
                                                if(zeiger){
                                                    partwahl.add(ch.getKey());
                                                } 
                                            }
                                        }
                                    } else if(counterEin==1){
                                        for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                            int index=s.lastIndexOf(" < ");
                                            index+=3;
                                            String wert=s.substring(index);
                                            System.out.println(wert);
                                            int counterW=0;
                                            ArrayList<String> cmp=ch.getValue();
                                            String w=cmp.get(0).replaceAll(" ","");
                                            if(w.contains(".")){
                                                w=w.substring(0, w.indexOf(".")-1);
                                            }
                                            //w=w.replace(".", "");
                                            int we = NumberFormat.getIntegerInstance().parse(w).intValue();
                                            w=Integer.toString(we);
                                            System.out.println(w);
                                            String w2=cmp.get(1).replaceAll(" ","");
                                            int we2= NumberFormat.getIntegerInstance().parse(w2).intValue();
                                            w2=Integer.toString(we2);
                                            if(w2.contains(".")){
                                                w2=w2.substring(0, w2.indexOf(".")-1);
                                            }
                                            System.out.println(w2);
                                            //w2=w2.replace(".", "");
                                            if((Integer.parseInt(w)<=Integer.parseInt(wert) && Integer.parseInt(wert)<=Integer.parseInt(w2))){// || (Integer.parseInt(w)>=Integer.parseInt(wert) && Integer.parseInt(wert)>=Integer.parseInt(w2)))){
                                                partwahl.add(ch.getKey());
                                            }
                                        }
                                    }
                                }
                            } else if(s.contains(" = ")){
                                for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                                    for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                        int index=s.lastIndexOf(" = ");
                                        index+=3;
                                        String wert=s.substring(index);
                                        wert=wert.replaceAll("'", "");
                                        boolean zeiger=false;
                                        ArrayList<String> cmp=ch.getValue();
 
                                        for(String zt : ch.getValue()){
                                            if(zt.equals(wert)){
                                                zeiger=true;
                                            }
                                            if(zeiger){
                                                partwahl.add(ch.getKey());
                                            } 
                                        }
                                    }
                                }
                            }
                        }
                    }
                    counter=0;         
                    Statement scheck = conn.createStatement();  
                    HashMap<String, ArrayList<String>> all = new HashMap<>();
                    
                    for(String ch : partwahl){
                        ArrayList<String> check = new ArrayList<>();
                        String ex = "select distinct attrname from advisorhorzpartstats where partname='"+ch+"'";
                        ResultSet rsch = scheck.executeQuery(ex);
                        while(rsch.next()){
                            check.add(rsch.getString(1));
                        }
                        all.put(ch, check);
                    }
                    int countercheck=0;
                    for(ArrayList<String> str : all.values()){
                        for(String strs : str){
                            for(String str2 : attribute){
                                if(str2.contains(strs)){
                                    countercheck++;
                                }
                            }
                        }
                        if(countercheck!=str.size()&&attribute.size()==2){
                            partwahl.clear();
                            result=sline;
                        }
                        countercheck=0;
                    }
                    
                    String[] res = new String[2];
                    if(!partwahl.isEmpty() && attribute.size()>1){
                        
                        HashMap<String, Integer> bestPart= new HashMap<>();
                        for(String s : partwahl){
                            int freq=Collections.frequency(partwahl, s);
                            bestPart.put(s, freq);
                        }
                        int max=0;
                        String bestpart="";
                        int countermax=0;
                        for(Map.Entry<String, Integer> mp : bestPart.entrySet()){//gibt beste Partition aus 
                            if(mp.getValue()>max){
                                max=mp.getValue();
                                bestpart=mp.getKey();
                                countermax++;
                            }
                        }
                        if(max>=1 && !name.contains("orders")){
                            StringBuilder strb=new StringBuilder(sline);    
                            int index=strb.lastIndexOf(name);    
                            strb=strb.replace(index,sline.length()+index,bestpart);    
                            sline = strb.toString();
                            System.out.println(sline);
                            res = sline.split(" where ");
                            result = res[0];
                        } else if(max>=1 && name.contains("orders")){
                            sline=sline.replaceFirst("orders", bestpart);
                            res = sline.split(" where ");
                            result = res[0];
                            //sline=sline.replaceAll(bestpart,name);
                            System.out.println(sline);
                        } else{
                            result=sline;
                        } 

                        
                    } else if(!partwahl.isEmpty() && attribute.size()==1){

                        String cur="";
                        int countercur=0;
                        for(String scur : partwahl){
                            countercur++;
                            String curS="Select count("+vert.get(0)+"), count("+vert.get(1)+") from ";
                            if(countercur==partwahl.size()){
                                cur+=curS+scur;
                            } else{
                                cur=curS+scur+" union all ";
                            }
                        }
                        result=cur;
                    } else if(partwahl.isEmpty() && attribute.size()==1 && result.isBlank() ){
                        String getH = "select distinct partname from advisorhorzpartstats where tablename='"+newtbl+"'";
                        
                        Statement s3 = conn.createStatement();
                        ResultSet rs = s3.executeQuery(getH);
                        HashMap<String, ArrayList<String>> sm = new HashMap<>();
                        ArrayList<String> cur2 = new ArrayList<>();
                        while(rs.next()){
                            String nm = rs.getString(1);
                            cur2.add(nm);
                            System.out.println(nm);   
                        }
                        
                        String cur="";
                        int countercur=0;
                        System.out.println(cur2.size());
                        for(String scur : cur2){
                            countercur++;
                            System.out.println(countercur);
                            String curS="Select count("+vert.get(0)+"), count("+vert.get(1)+") from ";
                            if(countercur==cur2.size()){
                                cur+=curS+scur;
                            } else{
                                cur+=curS+scur+" union all ";
                            }
                        }
                        result=cur;
                    }

                    System.out.println(result);
                    long startTime = System.currentTimeMillis();
                    Statement a = conn.createStatement(); 
                    ResultSet r = a.executeQuery(result);
                    long endTime = System.currentTimeMillis();
                    r.close();
                    System.out.println(counterAus+". Anfrage: PostgreSQL " + (endTime- startTime) +" ms");
                    a.close();
                    completeTime+=(endTime- startTime);
                    curList.clear();
                    speicher.clear();
                } else{
                    System.out.println(sline);
                    long startTime = System.currentTimeMillis();
                    Statement a = conn.createStatement(); 
                    ResultSet r = a.executeQuery(sline);
                    long endTime = System.currentTimeMillis();
                    r.close(); 
                    System.out.println(counterAus+". Anfrage: PostgreSQL " + (endTime- startTime) +" ms");
                    completeTime+=(endTime- startTime);
                }
                counterAus++;
                result="";
            }
            System.out.println(completeTime);
            br.close();
        } catch(SQLException s){
            s.printStackTrace();
        }
    }
    //Ausführung für HP-VP, funktioniert identisch außer dass zu erst die advisorhorzpartstats Tabelle und dann dafür die advisorvertpartstats Tabelle durchlaufen wird
    public static void executeQueriesReverse(Connection conn) throws FileNotFoundException, SQLException, IOException, ParseException{
        conn.setAutoCommit(false);  
        int counter=0;
        int completeTime=0;
        String result="";
        try {BufferedReader br = new BufferedReader(new FileReader("anfrageneinzeln.txt"));
            String sline="";
          
            ArrayList<String> vert = new ArrayList<>();
            int counterAus=1;
            while((sline=br.readLine()) != null){
                counter++;
                HashMap<String, Integer> curList = new HashMap<>();
                
                ArrayList<String> list=new ArrayList<>();
                HashMap<Integer, HashMap<String, ArrayList<String>>> speicher = new HashMap<>();
                String name=(horzpartreverse.tablename(sline));
                vert = horzpartreverse.attwhere(name, sline);
                System.out.println(vert);
                int counterIndex=0;
                String[] cursplit=new String[2];
                for(String ar : vert){
                    if(ar.contains("<") || ar.contains("<=")){
                        if(ar.contains("<=")){
                            cursplit = ar.split(" <=");

                        }else{
                        cursplit = ar.split(" < ");

                        }

                    } else if(ar.contains(">") || ar.contains(">=")){
                        cursplit= new String[2];
                        if(ar.contains(">=")){
                            cursplit = ar.split(" >=");
                        }else{
                        cursplit = ar.split(" >");
                        }
                    } else if(ar.contains(" = ")){
                        cursplit=ar.split(" = ");

                    }
                    String getH = "select partname from advisorhorzpartstats where tablename='"+name+"' and attrname='"+cursplit[0]+"'";// and where attrname="+attname);
                    Statement s3 = conn.createStatement();
                    ResultSet rs = s3.executeQuery(getH);
                    HashMap<String, ArrayList<String>> sm = new HashMap<>();
                    ArrayList<String> cur2 = new ArrayList<>();
                    String cur="";
                    while(rs.next()){
                        String nm = rs.getString(1);
                        cur2.add(nm);
                    }
                    
                    
                    for(String i : cur2){
                        String getZ = "select wert from advisorhorzpartstats where tablename='"+name+"' and attrname='"+cursplit[0]+"' and partname='"+i+"'";// and where attrname="+attname);
                        Statement s4 = conn.createStatement();
                        ResultSet r4 = s4.executeQuery(getZ);
                        ArrayList<String> werte = new ArrayList<>();
                        while(r4.next()){
                            werte.add(r4.getString(1));
                        }
                        sm.put(i, werte);
                    }
                    speicher.put(counter, sm);
                    counter++;
                }
                System.out.println(speicher);
                ArrayList<String> partwahl = new ArrayList<>();
                for(String s : vert){
                    if(s.contains(" > ")){
                        int counterEin=0;
                        for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                            counterEin++;
                            if(counterEin==2){
                                for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                    int index=s.lastIndexOf(" > ");
                                    index+=3;
                                    
                                    String wert=s.substring(index);
                                    
                                    
                                    boolean zeiger=true;
                                    for(String zt : ch.getValue()){
                                        
                                        if(zt.contains(wert)){
                                            zeiger=false;
                                        }
                                        if(zeiger){
                                            partwahl.add(ch.getKey());
                                        } 
                                    }
                                }
                            } else if(counterEin==1){
                                for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                    int index=s.lastIndexOf(" > ");
                                    index+=3;
                                    
                                    String wert=s.substring(index);
                                    System.out.println(wert);
                                    int counterW=0;
                                    boolean zeiger=true;
                                    ArrayList<String> cmp=ch.getValue();
                                    String w=cmp.get(0).replaceAll(" ","");
                                    w=w.replace(".", "");
                                    String w2=cmp.get(1).replaceAll(" ","");
                                    w2=w2.replace(".", "");
                                    wert=wert.replaceAll(" ","");
                                    if((Integer.parseInt(wert)<=Integer.parseInt(w) && Integer.parseInt(wert)<=Integer.parseInt(w2)) || (Integer.parseInt(w)>=Integer.parseInt(wert) && Integer.parseInt(wert)>=Integer.parseInt(w2))){
                                        partwahl.add(ch.getKey());
                                    }
                                }
                            }
                        }
                    } else if(s.contains(">=")){
                        int counterEin=0;
                        for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                            counterEin++;
                            if(counterEin==2){
                                for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                    int index=s.lastIndexOf(" >= ");
                                    index+=3;
                                    
                                    String wert=s.substring(index);
                                    
                                    int counterW=0;
                                    boolean zeiger=true;
                                    for(String zt : ch.getValue()){
                                        counterW++;
                                        if(zt.contains(wert)){
                                            zeiger=false;
                                        }
                                        if(zeiger){
                                            partwahl.add(ch.getKey());
                                        } 
                                    }
                                }
                            } else if(counterEin==1){
                                for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                    int index=s.lastIndexOf(" >= ");
                                    index+=3;
                                    
                                    String wert=s.substring(index);
                                    
                                    wert=wert.replace(" ", "");
                                    int counterW=0;
                                    boolean zeiger=true;
                                    ArrayList<String> cmp=ch.getValue();
                                    String w=cmp.get(0).replaceAll(" ","");
                                    w=w.replace(".", "");
                                    String w2=cmp.get(1).replaceAll(" ","");
                                    w2=w2.replace(".", "");
                                    //wert=wert.replaceAll(".","");
                                    if(Integer.parseInt(wert)<=Integer.parseInt(w) && Integer.parseInt(wert)<=Integer.parseInt(w2)){
                                        partwahl.add(ch.getKey());
                                    }
                                }
                            }
                        }
                    } else if(s.contains("<=")){
                        int counterEin=0;
                        for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                            counterEin++;
                            if(counterEin==2){
                                for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                    int index=s.lastIndexOf(" <= ");
                                    index+=3;
                                    
                                    String wert=s.substring(index);

                                    boolean zeiger=true;
                                    for(String zt : ch.getValue()){
                                        
                                        if(zt.contains(wert)){
                                            zeiger=false;
                                        }
                                        if(zeiger){
                                            partwahl.add(ch.getKey());
                                        } 
                                    }
                                }
                            } else if(counterEin==1){
                                for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                    int index=s.lastIndexOf(" <= ");
                                    index+=3;
                                    
                                    String wert=s.substring(index);
                                    
                                    int counterW=0;
                                    boolean zeiger=true;
                                    ArrayList<String> cmp=ch.getValue();
                                    if((Integer.parseInt(cmp.get(0))<=Integer.parseInt(wert) && Integer.parseInt(wert)<=Integer.parseInt(cmp.get(1)) || (Integer.parseInt(cmp.get(0))>=Integer.parseInt(wert) && Integer.parseInt(wert)>=Integer.parseInt(cmp.get(1))))){
                                        partwahl.add(ch.getKey());
                                    }
                                }
                            }
                        }
                    } else if(s.contains(" < ")){
                        int counterEin=0;
                        for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                            counterEin++;
                            if(counterEin==2){
                                for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                    int index=s.lastIndexOf(" < ");
                                    index+=3;
                                    String wert=s.substring(index);
                                    int counterW=0;
                                    boolean zeiger=true;
                                    for(String zt : ch.getValue()){
                                        counterW++;
                                        if(zt.contains(wert)){
                                            zeiger=false;
                                        }
                                        if(zeiger){
                                            partwahl.add(ch.getKey());
                                        } 
                                    }
                                }
                            } else if(counterEin==1){
                                for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                    int index=s.lastIndexOf(" < ");
                                    index+=3;
                                    String wert=s.substring(index);
                                    ArrayList<String> cmp=ch.getValue();
                                    String w=cmp.get(0).replaceAll(" ","");
                                    if(w.contains(".")){
                                        w=w.substring(0, w.indexOf(".")-1);
                                    }
                                    int we = NumberFormat.getIntegerInstance().parse(w).intValue();
                                    w=Integer.toString(we);
                                    String w2=cmp.get(1).replaceAll(" ","");
                                    int we2= NumberFormat.getIntegerInstance().parse(w2).intValue();
                                    w2=Integer.toString(we2);
                                    if(w2.contains(".")){
                                        w2=w2.substring(0, w2.indexOf(".")-1);
                                    }
                                    if((Integer.parseInt(w)<=Integer.parseInt(wert) && Integer.parseInt(wert)<=Integer.parseInt(w2))){// || (Integer.parseInt(w)>=Integer.parseInt(wert) && Integer.parseInt(wert)>=Integer.parseInt(w2)))){
                                        partwahl.add(ch.getKey());
                                    }
                                }
                            }
                        }
                    } else if(s.contains(" = ")){
                        for(Map.Entry<Integer, HashMap<String, ArrayList<String>>> a : speicher.entrySet()){
                            for(Map.Entry<String, ArrayList<String>> ch : a.getValue().entrySet()){
                                int index=s.lastIndexOf(" = ");
                                index+=3;
                                String wert=s.substring(index);
                                wert=wert.replaceAll("'", "");
                                int counterW=0;
                                boolean zeiger=false;
                                ArrayList<String> cmp=ch.getValue();

                                for(String zt : ch.getValue()){
                                    if(zt.equals(wert)){
                                        zeiger=true;
                                    }
                                    if(zeiger){
                                        partwahl.add(ch.getKey());
                                    } 
                                }
                            }
                        }
                    }
                }
                System.out.println(partwahl);
                if(partwahl.size()==1){
                    int counterCheck=0;
                    String getH = "select distinct attrname from advisorhorzpartstats where tablename='"+name+"' and partname='"+partwahl.get(0)+"'";// and where attrname="+attname);
                    System.out.println(getH);
                    Statement s3 = conn.createStatement();
                    ResultSet rs = s3.executeQuery(getH);
                    ArrayList<String> cur2 = new ArrayList<>();
                    while(rs.next()){
                        String nm = rs.getString(1);
                        cur2.add(nm);
                    }
                    System.out.println(cur2);
                    for(String s : cur2){
                        for(String s2 : vert){
                            if(s2.contains(s)){
                                counterCheck++;
                            }
                        }
                    }
                    if(counterCheck!=cur2.size()){
                        partwahl.clear();
                    }
                }
                String newtbl ="";
                if(!partwahl.isEmpty()){
                    HashMap<String, Integer> wahl = new HashMap<>();
                    for(String c : partwahl){
                        if(wahl.containsKey(c)){
                            wahl.put(c,wahl.get(c)+1);
                        }else{
                            wahl.put(c,1);
                        }
                    }
                    int max=0;
                    String bestpart="";
                    for(Map.Entry<String, Integer> mp : wahl.entrySet()){
                        if(max<mp.getValue()){
                            max=mp.getValue();
                            bestpart=mp.getKey();
                        }
                    }
                    ArrayList<String> attrcol = new ArrayList<>();
                    attrcol=horzpartreverse.getVertName(sline);
                    int counterindattr=0;
                    for(String ab : attrcol){
                        counterindattr++;
                        ab=ab.replace("count(", "");
                        ab=ab.replace(")","");
                        attrcol.set(counterindattr-1, ab);
                    }
                    for(String a : attrcol){
                        String f = "select partname from advisorvertpartstats where tablename=? and partattr=?";
                        PreparedStatement s = conn.prepareStatement(f);
                        s.setString(1, bestpart);
                        s.setString(2, a);
                        
                        ResultSet rs = s.executeQuery();
                        String pname="";
                        
                        while(rs.next()){
                            pname=rs.getString(1);

                            if(curList.containsKey(pname)){
                                curList.put(pname, curList.get(pname)+1);
                            } else {
                                curList.put(pname, 1);
                            }
                        }
                    }

                    
                    int countertbl=1;
                    boolean pointer=true;
                    for(String s : curList.keySet()){
                        if(curList.get(s)>1){
                            newtbl=s;
                        }
                    }
                    System.out.println(newtbl);
                }
                
                if(!newtbl.isEmpty()){
                    if(!sline.contains("orders")){
                        String[] res = new String[2];
                    StringBuilder strb=new StringBuilder(sline);    
                    int index=strb.lastIndexOf(name);    
                    strb.replace(index,sline.length()+index,newtbl);    
                    sline = strb.toString();
                    System.out.println(sline);
                    res = sline.split(" where ");
                    result = res[0];
                    
                    System.out.println(result);
                    long startTime = System.currentTimeMillis();
                    Statement a = conn.createStatement(); 
                    ResultSet r = a.executeQuery(result);
                    long endTime = System.currentTimeMillis();
                    System.out.println(counterAus+". Anfrage: PostgreSQL " + (endTime- startTime) +" ms");
                    r.close(); 
                    a.close();
                    counterAus++;
                    completeTime+=(endTime- startTime);
                    curList.clear();
                    speicher.clear();
                    } else if(sline.contains("orders")){
                        String[] res = new String[2];
                        StringBuilder strb=new StringBuilder(sline);    
                        int index=strb.indexOf(name);    
                        strb.replace(index,sline.length()+index,newtbl);    
                        sline = strb.toString();
                        System.out.println(sline);
                        res = sline.split(" where ");
                        result = res[0];

                        System.out.println(result);
                        long startTime = System.currentTimeMillis();
                        Statement a = conn.createStatement(); 
                        
                        ResultSet r = a.executeQuery(result);
                                
                        long endTime = System.currentTimeMillis();
                        System.out.println(counterAus+". Anfrage: PostgreSQL " + (endTime- startTime) +" ms");
                        r.close(); 
                        a.close();
                        counterAus++;
                        completeTime+=(endTime- startTime);
                        curList.clear();
                        speicher.clear();
                    }
                } else {
                    System.out.println(sline);
                    long startTime = System.currentTimeMillis();
                    Statement a = conn.createStatement(); 
                    ResultSet r = a.executeQuery(sline);
                    long endTime = System.currentTimeMillis();
                    System.out.println(counterAus+". Anfrage: PostgreSQL " + (endTime- startTime) +" ms");
                    completeTime+=(endTime- startTime);
                    counterAus++;
                    r.close();
                }
            }
        
            br.close();
            System.out.println(completeTime);
        } catch(SQLException s){
            s.printStackTrace();
        }
    }
    //executeQueriesDuck sucht nur nach passender HP
    public static void executeQueriesDuck(Connection conn)throws SQLException, IOException{
        try{BufferedReader br = new BufferedReader(new FileReader("anfrageneinzeln.txt"));
        String sline="";
        ArrayList<String> attrlist=new ArrayList<>();
        String result="";
        ArrayList<String> list = new ArrayList<>();
        int counterAus=1;
        int completeTime=0;
        while((sline=br.readLine()) != null){
            
            HashMap<String, Integer> curList = new HashMap<>();
            HashMap<Integer, HashMap<String, ArrayList<String>>> speicher = new HashMap<>();
            String name=(horzpartDuckDb.tablename(sline));
            attrlist = horzpartDuckDb.attwhere(name, sline);
            HashMap<String, HashMap<String, Integer>> sm = new HashMap<>();
            for(String ar : attrlist){
                
                curList=getPartname(conn, curList, ar, name);
                sm.put(ar, curList);
            }           
            
            int max=0;
            String bestpart="";
            int countermax=0;
            for(Map.Entry<String, Integer> mp : curList.entrySet()){
                if(mp.getValue()>max){
                    max=mp.getValue();
                    bestpart=mp.getKey();
                    countermax++;
                }
            }
            if(max>0){
                String[] res = new String[2];
                StringBuilder strb=new StringBuilder(sline);
                if(sline.contains("orders")){
                    int index=strb.indexOf(name,2);
                    strb.replace(index,sline.length()+index,bestpart);    
                    sline = strb.toString();
                    System.out.println(sline);
                    res = sline.split(" where ");
                    result = res[0];
                } else{
                    int index=strb.lastIndexOf(name);    
                    strb.replace(index,sline.length()+index,bestpart);    
                    sline = strb.toString();
                    System.out.println(sline);
                    res = sline.split(" where ");
                    result = res[0];
                }
            } else {
                result=sline;
            }
            
            System.out.println(result);
            long startTime = System.currentTimeMillis();
            Statement a = conn.createStatement(); 
            ResultSet r = a.executeQuery(result);
            long endTime = System.currentTimeMillis();
            r.close();
            System.out.println(counterAus+". Anfrage: DuckDB " + (endTime- startTime) +" ms");
            counterAus++;
            a.close();
            completeTime+=(endTime- startTime);

        }
        System.out.println(completeTime);
    } catch(SQLException s){
        s.printStackTrace();
    }
}

//Liste mit partnamen und deren Häufigkeit
public static HashMap<String, Integer> getPartname(Connection conn, HashMap<String, Integer> curList, String ar, String name) throws SQLException{
    String wert="";
    String attr="";
    System.out.println(ar);
    if(ar.contains(" < ")){
                        
        wert=ar.substring(ar.lastIndexOf("< ")+1);
        attr=ar.replaceAll(wert, "");
        attr=attr.replace("<", "");
        attr=attr.replace(" ", "");
        wert=wert.replace("'", "");
        wert=wert.replace(" ", "");

        Statement s = conn.createStatement();

        ResultSet rs = s.executeQuery("select partname from advisorhorzpartstats where tablename='"+name+"' and attrname='"+attr+"' and wert<"+wert);
        String pname="";
        while(rs.next()){
            pname=rs.getString(1);
            if(curList.containsKey(pname)){
                curList.put(pname, curList.get(pname)+1);
            } else {
                curList.put(pname, 1);
            }
        }
    } else if(ar.contains(" <= ")){
        
        wert=ar.substring(ar.lastIndexOf("<= ")+1);
        attr=ar.replaceAll(wert, "");
        attr=attr.replace("<=", "");
        attr=attr.replace(" ", "");
        wert=wert.replace("'", "");
        wert=wert.replaceFirst(" ", "");

        Statement s = conn.createStatement();
        String f = "select partname from advisorhorzpartstats where tablename='"+name+"' and attrname='"+attr+"' and wert<="+wert;
        ResultSet rs = s.executeQuery(f);
        String pname="";
        while(rs.next()){
            pname=rs.getString(1);

            if(curList.containsKey(pname)){
                curList.put(pname, curList.get(pname)+1);
            } else {
                curList.put(pname, 1);
            }
        }
    } else if(ar.contains(" >= ")){
        
        wert=ar.substring(ar.lastIndexOf(">= ")+3);
        attr=ar.replaceAll(wert, "");
        attr=attr.replace(">=", "");
        attr=attr.replace(" ", "");
        wert=wert.replace("'", "");
        wert=wert.replaceFirst(" ", "");
        Statement s = conn.createStatement();
        String f = "select partname from advisorhorzpartstats where tablename='"+name+"' and attrname='"+attr+"' and wert>="+wert;
        ResultSet rs = s.executeQuery(f);
        String pname="";
        while(rs.next()){
            pname=rs.getString(1);
            if(curList.containsKey(pname)){
                curList.put(pname, curList.get(pname)+1);
            } else {
                curList.put(pname, 1);
            }
        }
    } else if(ar.contains(" > ")){
        
        wert=ar.substring(ar.lastIndexOf("> ")+1);
        attr=ar.replaceAll(wert, "");
        attr=attr.replace(">", "");
        attr=attr.replace(" ", "");
        wert=wert.replace("'", "");
        wert=wert.replaceFirst(" ", "");
        Statement s = conn.createStatement();
        String f = "select partname from advisorhorzpartstats where tablename='"+name+"' and attrname='"+attr+"' and wert>"+wert;
        ResultSet rs = s.executeQuery(f);
        String pname="";
        while(rs.next()){
            pname=rs.getString(1);
            if(curList.containsKey(pname)){
                curList.put(pname, curList.get(pname)+1);
            } else {
                curList.put(pname, 1);
            }
        }
    } else if(ar.contains(" = ")){
        
        wert=ar.substring(ar.lastIndexOf("=")+1);
        attr=ar.replaceAll(wert, "");
        attr=attr.replace("=", "");
        attr=attr.replace(" ", "");
        wert=wert.replace("'", "");
        wert=wert.replaceFirst(" ", "");
        Statement s = conn.createStatement();
        String f = "select partname from advisorhorzpartstats where tablename='"+name+"' and attrname='"+attr+"' and werte='"+wert+"'";
        ResultSet rs = s.executeQuery(f);
        String pname="";
        while(rs.next()){
            pname=rs.getString(1);

            if(curList.containsKey(pname)){
                curList.put(pname, curList.get(pname)+1);
            } else {
                curList.put(pname, 1);
            }
        }
    }
    return curList;
}
        


}




