import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyStore.Entry;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class horizpart {
    public static void main(String[] args, Connection conn)throws IOException, SQLException{
        HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store = new HashMap<>();
  
        store = appmatrix(conn);
        if(store.containsKey("")){
            store.remove("");
        }
        System.out.println(store);
        for(String s : store.keySet()){
            HashMap<Integer, String> res = new HashMap<>();
            res = IDAttr(store, s);
            HashMap<ArrayList<String>, int[]> matrix1 = new HashMap<>();
            matrix1=firstMatrix(res, store, s);
            int [][] curres = getFinalMatrix(res, matrix1);
            ArrayList<Integer> cur = heuristic.getOrderMax(curres);            
            ArrayList<Integer> reihenfolge=heuristic.getIDMax(curres, cur);
            HashMap<Integer, ArrayList<Integer>> completeList=heuristic.innerMax(heuristic.getIDMax(curres, cur), curres);
            ArrayList<ArrayList<Integer>> parts =heuristic.createHortPart(conn, store, reihenfolge, completeList, s, res);
            System.out.println(parts);
            ArrayList<String> namensliste= new ArrayList<>();
            ArrayList<String> namenslisteCounterPart= new ArrayList<>();
            ArrayList<String> namensliste2= new ArrayList<>();
            ArrayList<String> namenslisteCounterPart2= new ArrayList<>();
            ArrayList<String> cross1 = new ArrayList<>();
            ArrayList<String> cross2 = new ArrayList<>();
            int counter=1;
            for(ArrayList<Integer> al : parts){
                if(al.size()==2){
                    ArrayList<Integer> cur2 = new ArrayList<>();
                    cur2.add(al.get(0));
                    ArrayList<Integer> cur3 = new ArrayList<>();
                    cur3.add(al.get(1));
                    namensliste=getAttName(res, cur2);
                    namenslisteCounterPart=createCounterPart(namensliste, conn, s);
                    namensliste2=getAttName(res, cur3);
                    namenslisteCounterPart2=createCounterPart(namensliste2, conn, s);
                    cross1.add(namensliste.get(0));
                    cross1.add(namenslisteCounterPart2.get(0));
                    cross2.add(namenslisteCounterPart.get(0));
                    cross2.add(namensliste2.get(0));
                    splitValue(cross1, conn, s, counter);
                    counter++;
                    splitValue(cross2, conn, s, counter);
                    counter++;
                    namensliste=getAttName(res, al);
                    splitValue(namensliste, conn, s, counter);
                    counter++;
                    namensliste=createCounterPart(namensliste, conn, s);
                    splitValue(namensliste, conn, s, counter);
                    counter++;
                } else if(al.size()==1){
                    namensliste=getAttName(res, al);
                    splitValue(namensliste, conn, s, counter);
                    counter++;
                    namensliste=createCounterPart(namensliste, conn, s);
                    splitValue(namensliste, conn, s, counter);
                    counter++;
                }
            }     
        }
    }
    public static HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> appmatrix(Connection conn)throws IOException, SQLException, NullPointerException{
        HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store = new HashMap<>(); 
        ConcurrentHashMap<ArrayList<String>, Integer> score = new ConcurrentHashMap<>();
        ArrayList<String> attr = new ArrayList<>();
        ArrayList<String> vert = new ArrayList<>();
        int counterA=0;
        int counter=1;
        int counterS=1;
        String line="";
        BufferedReader br = new BufferedReader(new FileReader("anfragenhoriz.txt"));
        while(((line =br.readLine())!=null)){
            String name=(tablename(line));
            vert = getVertName(line);
            int counterIndex=0;
            System.out.println(vert);
            for(String ab : vert){
                counterIndex++;
                ab=ab.replace("count(", "");
                ab=ab.replace(")","");
                vert.set(counterIndex-1, ab);
            }
            ArrayList<String> lookup2=new ArrayList<>();
            String[] lookup = new String[vert.size()];
            ArrayList<String> partnamen = new ArrayList<>();
            for(String c : vert){
                Statement s = conn.createStatement();
                ResultSet rs = s.executeQuery("select partname from advisorvertpartstats where tablename='"+name+"' and partattr='"+c+"'");
                String pname="";
                while(rs.next()){
                    pname = rs.getString(1);
                    lookup2.add(pname);
                }
            }
            HashMap<String, Integer> bestPart= new HashMap<>();
            for(String s2 : lookup2){
                int freq=Collections.frequency(lookup2, s2);
                bestPart.put(s2, freq);
            }
            int max=0;
            String bestpart="";
            for(Map.Entry<String, Integer> mp : bestPart.entrySet()){
                if(mp.getValue()>max){
                    max=mp.getValue();
                    bestpart=mp.getKey();
                }
            }

            attr=attwhere(name, line);
            String partname=bestpart;
            if(store.containsKey(partname)){
                Set<ArrayList<String>> curr = store.get(partname).keySet();
                if(curr.contains(attr)){
                    counter=store.get(partname).get(attr)+1;
                    store.get(partname).put(attr, counter);
                }else{
                    store.get(partname).put(attr, counter);

                }
                counter=1;
            }else {
                ConcurrentHashMap<ArrayList<String>, Integer> scoreNeu = new ConcurrentHashMap<>();
                scoreNeu.put(attr,counterS);
                store.put(partname, scoreNeu); 
            }
            
        }
        br.close();
        return store;
    }
    public static HashMap<Integer, String> IDAttr(HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String tblname){
        int counterA=1;
        HashMap<Integer, String> speicher = new HashMap<>();
        for(ArrayList<String> a : store.get(tblname).keySet()){
            for(String d : a){
                if(!speicher.containsValue(d)){
                    speicher.put(counterA, d);
                    counterA++;
                }
            }
        }
        return speicher;
    }

    public static HashMap<ArrayList<String>, int[]> firstMatrix(HashMap<Integer, String> speicher, HashMap<String, ConcurrentHashMap<ArrayList<String>, Integer>> store, String tblname){
        HashMap<ArrayList<String>, int[]> matrix = new HashMap<>();
        int i=0;
        for(ArrayList<String> a : store.get(tblname).keySet()){
            int [] appe = new int[speicher.size()];
            for(String y : speicher.values()){
                if(a.contains(y)){
                    appe[i]=1*Integer.valueOf(store.get(tblname).get(a));
                } else{
                    appe[i]=0;
                }
                i++;
            }
            i=0;
            matrix.put(a, appe);
        }
        return matrix;
    }
    public static int[][] getFinalMatrix(HashMap<Integer, String> speicher, HashMap<ArrayList<String>, int[]> matrix){// zum implizieren von Attributen, wenn ein Attribut das andere impliziert bekommt es dessen Punkte auch
        int[][] PAMmatrix = new int[speicher.size()][speicher.size()];
        int nrRow=0;
        int nrLine=0;
        for(String s : speicher.values()){
            for(Map.Entry<Integer, String> a : speicher.entrySet()){
                if(a.getValue().contains(s)){
                    nrRow=Integer.valueOf(a.getKey());
                }
            }
            for(Map.Entry<ArrayList<String>,int[]> h : matrix.entrySet()){
                if(h.getKey().contains(s)){
                    for(String b : h.getKey()){
                        for(Map.Entry<Integer, String> a2 : speicher.entrySet()){
                            if(a2.getValue().contains(b)){
                                nrLine=Integer.valueOf(a2.getKey());
                                int cur[]=matrix.get(h.getKey());
                                PAMmatrix[nrRow-1][nrLine-1]+=cur[nrLine-1];
                            }
                        }
                    }
                }
            } 
        }
        return PAMmatrix;
    }
    public static ArrayList<String> selectBestPart(int[][] PAMmatrix, HashMap<Integer, String> speicher){
        ArrayList<Integer> cur = findMax(PAMmatrix);
        ArrayList<String> res = new ArrayList<>();
        for(Integer i : cur){
            for(Map.Entry<Integer, String> s : speicher.entrySet()){
                if(s.getKey().equals(i)){
                    res.add(s.getValue());
                }
            }
        }
        return res;
    }
    public static ArrayList<String> createCounterPart(ArrayList<String> res, Connection conn, String tblname) throws SQLException{
        ArrayList<String> alt= new ArrayList<>();
        Statement ct = conn.createStatement();
        ResultSet rscur = ct.executeQuery("select tablename from advisorvertpartstats where partname='"+tblname+"'");
            String origintbl="";
            while(rscur.next()){
                origintbl=rscur.getString(1);
            }
        for(String s : res){
            if(s.contains(" > ")){
                s=s.replaceAll(">", "<=");
                alt.add(s);
                
            } else if(s.contains(">=")){
                s=s.replaceAll(">=", "<");
                alt.add(s);
                
            }else if(s.contains(" < ")){
                s=s.replaceAll("<", ">=");
                System.out.println(s);
                alt.add(s);
                
            } else if(s.contains("<=")){
                s=s.replaceAll("<=", ">");
                alt.add(s);
                
            } else if(s.contains(" = ")){//Achtung hier sind somit nur = Abfragen bei Attribute die nur 2 versch. Werte besitzen, möglich
                String[] cur = s.split("=");
                String attr = cur[0];
                String val = cur[1];
                val=val.replaceAll("'", "");
                val=val.replaceFirst(" ", "");
                String q="Select "+attr+" from "+ origintbl+" where "+attr+" not like '"+val+"' limit 1";
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(q);
                String valrp="";
                while(rs.next()){
                    valrp = rs.getString(1);
                }
                
                s=s.replace(val,valrp);
                alt.add(s);
            }
        }
        
        return alt;
    }
    public static void splitValue(ArrayList<String> best, Connection conn, String tblname, int counter)throws SQLException, IOException{
        //baut Statistiktabelle auf
        String q="";
        String partname = tblname+"h"+counter;
        String min="";
        String max="";
        
        Statement ct=conn.createStatement();
        ResultSet rscur = ct.executeQuery("select tablename from advisorvertpartstats where partname='"+tblname+"'");
            String origintbl="";
            while(rscur.next()){
                origintbl=rscur.getString(1);
            }
        for(String s : best){
            int counterCur=0;
            if(s.contains("<") || s.contains("<=")){
                String[] cursplit= new String[2];
                if(s.contains("<=")){
                    cursplit = s.split(" <=");
                }else{
                cursplit = s.split(" < ");
                }
                String attr=cursplit[0];
                for(String t : cursplit){
                    counterCur++;
                    if(counterCur==2){
                        String sd = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                        PreparedStatement ps = conn.prepareStatement(sd);
                        ps.setString(1, tblname);
                        ps.setString(2, partname);
                        ps.setString(3, attr);
                        t=t.replace(" ", "");
                        ps.setString(4, t);
                        ps.executeUpdate();
                    } else {
                        String q1="Select min("+attr+") from "+origintbl;
                        Statement st = conn.createStatement();
                        ResultSet rs = st.executeQuery(q1);
                        while(rs.next()){
                            
                            min =rs.getString(1);
                        }
                        String sd = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                        PreparedStatement ps = conn.prepareStatement(sd);
                        ps.setString(1, tblname);
                        ps.setString(2, partname);
                        ps.setString(3, t);
                        ps.setString(4, min);
                        ps.executeUpdate();
                    } 
                } 
            } else if(s.contains(">") || s.contains(">=")){
                String[] cursplit= new String[2];
                if(s.contains(">=")){
                    cursplit = s.split(" >=");
                }else{
                cursplit = s.split(" >");
                }
                System.out.println(Arrays.toString(cursplit));
                String attr=cursplit[0];
                for(String t : cursplit){
                    t=t.replace(" ","");
                    counterCur++;
                    if(counterCur==2){
                        String sd = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                        PreparedStatement ps = conn.prepareStatement(sd);
                        ps.setString(1, tblname);
                        ps.setString(2, partname);
                        ps.setString(3, attr);
                        //int tw=Integer.valueOf(t);
                        //tw++;
                        ps.setString(4, t);
                        ps.executeUpdate();
                    } else {
                        String q1="Select max("+attr+") from "+origintbl;
                        Statement st = conn.createStatement();
                        ResultSet rs = st.executeQuery(q1);
                        while(rs.next()){
                            max=rs.getString(1);
                            
                        }
                        String sd = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                        PreparedStatement ps = conn.prepareStatement(sd);
                        ps.setString(1, tblname);
                        ps.setString(2, partname);
                        ps.setString(3, t);
                        ps.setString(4, max);

                        ps.executeUpdate();
                    } 
                }
            
            } else if(s.contains(" = ")){
                String[] cur=s.split(" = ");
                String attr=cur[0];
                cur[1]=cur[1].replaceAll("'", "");
                String sd3 = "insert into advisorHorzpartstats values (?, ?, ?, ?)";
                PreparedStatement ps = conn.prepareStatement(sd3);
                ps.setString(1, tblname);
                ps.setString(2, partname);
                ps.setString(3, attr);
                ps.setString(4, cur[1]);

                ps.executeUpdate();
            }
        }
        createHort(best, conn, tblname, counter);
    }
    
    public static void createHort(ArrayList<String> ar, Connection conn, String tblname, int counter)throws SQLException, IOException{
        
        String g ="select tblpkey from advisorstats where tablename=?";
        ArrayList<String> keylist=new ArrayList<>();
        PreparedStatement pre = conn.prepareStatement(g);
        pre.setString(1, tblname);
        ResultSet rs = pre.executeQuery();
        while(rs.next()){
            String key = rs.getString(1);
            keylist.add(key);
        }
        System.out.println(keylist);
        ArrayList<String> attlisteName = new ArrayList<>();
        ArrayList<String> attliste = new ArrayList<>();
        String f ="select column_name, data_type, character_maximum_length from information_schema.columns where table_name=?"; //gibt datentyp und attributnamen von spezifischer tabelle
        PreparedStatement pre2 = conn.prepareStatement(f);
        pre2.setString(1, tblname);
        ResultSet daten = pre2.executeQuery();
        while(daten.next()){
            String resa = daten.getString(1);//attname
            String resb = daten.getString(2);//datentyp
            Integer leng = daten.getInt(3);//char anzahl
            attlisteName.add(resa);
            String att="";
            if(leng==0){
                att = resa +" "+resb;
            }else{
                att = resa +" "+resb+"("+leng+")";
            }
            
            attliste.add(att);
        }
        String create = "";//alle Attribute + deren Datentypen hintereinander
        int counterZ=0;
        for(String att: attliste){
            if(attliste.size()>=counterZ){
                create+=att+",";
                counterZ++;
            } else {
                create+=att;
            }
        }
        //entfernt Daten
        
        String keycre="Primary Key(";//erstellt Primary Key bezeichnung in create table statement
        int counterP=0;
        if(keylist.size()>1){
            for(String h : keylist){
                counterP++;
                if(counterP==keylist.size()){
                    keycre+=h;
                } else {
                    keycre+=h+","; 
                }
            }
        } else{
            for(String h : keylist){
                keycre+=h;
            }
        }
        String name ="";
        int counterN=1;
        for(String atname : attlisteName){
            if(attlisteName.size()>counterN){
                name+=atname+",";
                counterN++;
            }else {
                name+=atname;
            }
        }
        if(ar.size()>0){
            String a=ar.get(0);
            String wert="";
            if(ar.contains(" < ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf("< ")+1);

                a=a.replaceAll(wert, "");
            } else if(ar.contains(" <= ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf("<= ")+1);

                a=a.replaceAll(wert, "");
            } else if(ar.contains(" >= ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf(">= ")+1);

                a=a.replaceAll(wert, "");
            } else if(ar.contains(" > ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf("> ")+1);

                a=a.replaceAll(wert, "");
            } else if(ar.contains(" = ")){
                a=ar.get(0);
                wert=ar.get(0).substring(ar.get(0).lastIndexOf("=")+1);

                a=a.replaceAll(wert, "");
            }
            if(ar.size()>1){
                System.out.println("Hallo");
                String s=ar.get(1);
                Statement ct = conn.createStatement();
                String partname=tblname+"h"+counter;
                ct.executeUpdate("create table "+partname+"("+create+" "+keycre+"))");
                counter++;
                ResultSet rscur = ct.executeQuery("select tablename from advisorvertpartstats where partname='"+tblname+"'");
                String origintbl="";
                while(rscur.next()){
                    origintbl=rscur.getString(1);
                }
                String bsp="insert into "+partname+"("+name+") select "+name+" from "+origintbl+" where "+a+wert+" and "+s;
                ct.executeUpdate(bsp);

            }else{
                Statement ct = conn.createStatement();
                String partname=tblname+"h"+counter;
                ct.executeUpdate("create table "+partname+"("+create+" "+keycre+"))");
                counter++;
                ResultSet rscur = ct.executeQuery("select tablename from advisorvertpartstats where partname='"+tblname+"'");
                String origintbl="";
                while(rscur.next()){
                    origintbl=rscur.getString(1);
                }
                String bsp = "insert into "+partname+"("+name+") select "+name+" from "+origintbl+" where "+a+wert;
                ct.executeUpdate(bsp);
            }
        }
        
    }
    public static ArrayList<Integer> findMax(int[][] PAMMatrix){
        int max=0;
        int attr1=0;
        int attr2=0;
        ArrayList<Integer> res = new ArrayList<>();
        for(int i=0;i<PAMMatrix.length;i++){
            for(int j=0;j<PAMMatrix[i].length;j++){
                if(max < PAMMatrix[i][j] && i != j){
                    max=PAMMatrix[i][j];
                    attr1=i+1;
                    attr2=j+1;
                }
            }
            
        }
        res.add(attr1);
        res.add(attr2);
        return res;
    }

    public static String tablename(String s){
        String[] str = s.split("from ");
        for(String a : str){
            if(!a.contains("select")){
                if(a.contains("where")){
                    String[] cur = a.split(" where");
                    for(String f:cur){
                        if(!f.contains("where")){
                            return f;
                        }
                    }
                } else {
                    return a;
                }
            }
        }
        return "";
    }
    public static ArrayList<String> getVertName(String s){
        String[] str = s.split("from ");
        ArrayList<String> attr = new ArrayList<>();
        for(String a : str){
            if(!a.contains("select")){
                continue;
            } else if(a.contains("select")){
                String[] ar = a.split(" ");
                for(String h : ar){
                    if(!h.contains("select")){
                        String[] ar2 = h.split(",");
                        for(String k : ar2){
                            attr.add(k);
                        }
                        
                    }
                }
            }
        }
        return attr;
    }
    public static ArrayList<String> attwhere(String name, String line){
        ArrayList<String> attr=new ArrayList<>();
        String[] str = line.split(name, 2);
        System.out.println(str.length);
        for(String a : str){
            if(!a.contains("select")){
                if(a.contains("where")){
                    String[] cur = a.split(" where ");
                    List<String> cur2 = new LinkedList<String>(Arrays.asList(cur));
                    
                    cur2.remove(0);
                    for(String c: cur2){
                        if(c.contains(" and ")){
                            String[] curtwo= c.split(" and ");
                            for(String h : curtwo){
                                if(!h.contains(" and ")){
                                    attr.add(h);
                                }
                            }
                        } else{
                            attr.add(c);
                        } 
                    }
                } 
            }
        }
        return attr;
    }
    public static ArrayList<String> getAttName(HashMap<Integer, String> res, ArrayList<Integer> part){
        ArrayList<String> namensliste = new ArrayList<>();
        for(Integer i : part){
            for(Integer i2 : res.keySet()){
                if(Integer.valueOf(i+1).equals(i2)){
                    namensliste.add(res.get(i2));
                }
            }
        }
        return namensliste;
    }
}
